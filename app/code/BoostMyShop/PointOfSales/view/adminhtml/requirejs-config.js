var config = {
    map: {
        '*': {
            pos_ui:  'BoostMyShop_PointOfSales/js/ui',
            pos_checkout: 'BoostMyShop_PointOfSales/js/checkout',
            pos_scanner: 'BoostMyShop_PointOfSales/js/scanner',
            regionUpdater: 'Magento_Checkout/js/region-updater'
        }
    }
};