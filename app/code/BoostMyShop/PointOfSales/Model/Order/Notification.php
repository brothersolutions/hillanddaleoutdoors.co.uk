<?php

namespace BoostMyShop\PointOfSales\Model\Order;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use Magento\Payment\Helper\Data as PaymentHelper;

class Notification
{

    const XML_PATH_CUSTOM_EMAIL_TEMPLATE= 'pointofsales_checkout_email_templates';   // section_id/group_id/field_id

    protected $_logger;
    protected $_receipt;
    protected $_config;
    protected $_transportBuilder;
    protected $scopeConfig;
    protected $addressRenderer;
    protected $paymentHelper;
    protected $identityContainer;
    protected $_storeManager;

    public function __construct(
        \BoostMyShop\PointOfSales\Model\Pdf\Receipt $receipt,
        \BoostMyShop\PointOfSales\Model\Config $config,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \BoostMyShop\PointOfSales\Helper\Logger $logger,
        ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        OrderIdentity $identityContainer,
        PaymentHelper $paymentHelper

)
    {
        $this->scopeConfig = $scopeConfig;
        $this->_config = $config;
        $this->_transportBuilder = $transportBuilder;
        $this->_logger = $logger;
        $this->_receipt = $receipt;
        $this->addressRenderer = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->identityContainer = $identityContainer;
        $this->_storeManager = $storeManager;
    }

    public function notifyCustomer($order)
    {
        $storeId = $order->get_store_id();
        $template = self::XML_PATH_CUSTOM_EMAIL_TEMPLATE;
        $params = $this->buildParams($order);
        $params['pdf_url'] = $this->getDownloadPdfUrl($order);
        $params['order'] = $order;
        $params['payment_html'] = $this->getPaymentHtml($order);
        $params['formattedShippingAddress'] = $this->getFormattedShippingAddress($order);
        $params['formattedBillingAddress'] = $this->getFormattedBillingAddress($order);
        $pdfFile = $this->_receipt->getPdf([$order]);
        $this->_sendEmailTemplate($template, $params, $storeId, $order->getcustomer_email(), $order->getcustomer_name(),$pdfFile);
    }

    protected function _sendEmailTemplate($template, $templateParams = [], $storeId, $recipientEmail, $recipientName,$pdfFile = null)
    {
        $transport = $this->_transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            $templateParams
        )->setFrom(
            [
                'email' => $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE)
            ]
        )->addTo(
            $recipientEmail,
            $recipientName
        //)->addAttachment($pdfFile->render(), 'receipt'
        )->getTransport();
        $transport->sendMessage();
        return $this;
    }

    protected function getDownloadPdfUrl($order)
    {
        $token = sha1($order->getId().' order '.$order->getcreated_at());
        $url = $this->_storeManager->getStore($order->getstore_id())->getBaseUrl().'pointofsales/order/printReceipt/order_id/'.$order->getId().'/type/pdf/token/'.$token;
        return $url;
    }

    protected function buildParams($order)
    {
        $data = [];

        foreach ($order->getData() as $k => $v) {
            if (is_string($v))
                $data[$k] = $v;
        }

        if ($order->getOrder())
        {
            foreach($order->getOrder()->getData() as $k => $v)
            {
                if (is_string($v))
                    $data['order_'.$k] = $v;
            }
        }

        return $data;
    }

    protected function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    protected function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    protected function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }

}