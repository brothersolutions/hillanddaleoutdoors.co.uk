<?php

namespace BoostMyShop\PointOfSales\Block\Payment\MultiplePayment;

class Info extends \Magento\Payment\Block\Info
{
    protected $_template = 'BoostMyShop_PointOfSales::Payment/MultiplePayment/Info.phtml';
    protected $_paymentCollectionFactory;
    protected $orderRepository;
    protected $_config;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \BoostMyShop\PointOfSales\Model\ResourceModel\Payment\CollectionFactory $paymentCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \BoostMyShop\PointOfSales\Model\Config $config,
        array $data = []
    )
    {
        $this->_paymentCollectionFactory = $paymentCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->_config = $config;

        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('BoostMyShop_PointOfSales::Payment/MultiplePayment/Pdf/Info.phtml');
        return $this->toHtml();
    }

    public function getPayments()
    {
        $orderId = $this->getInfo()->getData('parent_id');
        return $this->_paymentCollectionFactory->create()->addOrderFilter($orderId);
    }

    public function getDuePayment()
    {
        $totalPaid = 0;
        $grandTotal = $this->getInfo()->getData('amount_ordered');
        $payments = $this->getPayments();
        foreach ($payments as $payment) {
            $totalPaid += $payment->getAmount();
        }
        $totalDue = (int)$grandTotal - (int)$totalPaid;

        return $totalDue;
    }

    public function getOrderIncrementId()
    {
        $orderId = $this->getInfo()->getData('parent_id');
        $order = $this->orderRepository->get($orderId);
        return $order->getIncrementId();
    }

    public function getPaymentMethods()
    {
        $html = '<select class="admin__control-select" id="multiple-payment-method">';
        $paymentMethods = $this->_config->getMultiplePaymentMethods();
        foreach($paymentMethods as $method) {
            $html .= '<option id="method-'.$method.'" value="'.$method.'">'.$method.'</option>';
        }
        $html .='</select>';

        return $html;
    }

    public function getSavePaymentUrl()
    {
        return $this->getUrl('pointofsales/payment/save');
    }
}
