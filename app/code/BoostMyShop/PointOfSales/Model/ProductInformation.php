<?php

namespace BoostMyShop\PointOfSales\Model;

use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;

class ProductInformation
{
    protected $_coreRegistry = null;
    protected $_productRepository;
    protected $_objectManager;
    protected $_stockState;
    protected $_stockRegistry;
    protected $_collectionFactory;
    protected $_config;
    protected $_catalogPriceRule;
    protected $_posRegistry;
    protected $_priceFactory;
    protected $_magentoConfig;
    protected $_groupManagement;

    public function __construct(ProductRepositoryInterface $productRepository,
                                ObjectManagerInterface $om,
                                \Magento\CatalogInventory\Api\StockStateInterface $stockState,
                                \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
                                \Magento\Catalog\Model\ResourceModel\Product\Collection $collectionFactory,
                                \BoostMyShop\PointOfSales\Model\Config $config,
                                \Magento\CatalogRule\Model\Rule $catalogPriceRule,
                                \BoostMyShop\PointOfSales\Model\Registry $posRegistry,
                                \Magento\Customer\Api\GroupManagementInterface $groupManagement,
                                \Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory $priceFactory,
                                \Magento\Framework\App\Config\ScopeConfigInterface $magentoConfig
    ) {
        $this->_objectManager = $om;
        $this->_productRepository = $productRepository;
        $this->_stockState = $stockState;
        $this->_stockRegistry = $stockRegistry;
        $this->_collectionFactory = $collectionFactory;
        $this->_config = $config;
        $this->_catalogPriceRule = $catalogPriceRule;
        $this->_posRegistry = $posRegistry;
        $this->_priceFactory = $priceFactory;
        $this->_magentoConfig = $magentoConfig;
        $this->_groupManagement = $groupManagement;
    }

    public function getImage($product)
    {
        if (is_numeric($product))
            $product = $this->_productRepository->getById($product);

        $helper = $this->_objectManager->get('\Magento\Catalog\Helper\Product');
        return $helper->getImageUrl($product);
    }

    public function getQty($product, $websiteId = 0)
    {
        return $this->_stockState->getStockQty($product->getId(), $websiteId);
    }

    public function getSellable($product, $websiteId = 0)
    {
        if($this->_config->AllowOutOfStockProductSale()){
            return true;
        }
        else {
            return $this->_stockRegistry->getProductStockStatus($product->getId(), $websiteId);
        }
    }

    public function getProductFinalPrice($product, $customerGroupId = null)
    {
        $finalPrice = $product->getfinal_price() ? : $product->getprice();

        try{
            $productInterface = $this->_productRepository->get($product->getSku(), ['edit_mode' => true]);
            $groupId = isset($customerGroupId) ? (int) $customerGroupId : $this->_posRegistry->getCurrentCustomerGroupId();

            //Get product tierPrice
            $priceKey = 'website_price';
            $value = $this->_magentoConfig->getValue('catalog/price/scope', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
            if ($value == 0) {
                $priceKey = 'price';
            }

            $groupId = ($groupId === 'all'
                ? $this->_groupManagement->getAllCustomersGroup()->getId()
                : $groupId);

            $prices = [];
            if($productInterface->getData('tier_price')){

                foreach ($productInterface->getData('tier_price') as $price) {

                    if ((is_numeric($groupId) && intval($price['cust_group']) === intval($groupId))
                        || ($groupId === 'all' && $price['all_groups'])
                    ) {
                        $tierPrice = $this->_priceFactory->create();
                        $tierPrice->setValue($price[$priceKey])
                            ->setQty($price['price_qty'])
                            ->setCustomerGroupId($groupId);
                        $prices[] = $tierPrice->getValue();
                    }
                }
            }

            $tierPrice = null;
            if(count($prices) > 0){
                $tierPrice = $prices[0];
            }

            $finalPrice = $tierPrice ? min($tierPrice, $finalPrice) : $finalPrice;

            //Get product ruledPrice
            $ruledPrice = null;
            $product->setCustomerGroupId($groupId);
            $ruledPrice = $this->_catalogPriceRule->calcProductPriceRule($product, $product->getPrice());

            $finalPrice = $ruledPrice ? min($ruledPrice, $finalPrice) : $finalPrice;

        }catch (\Exception $ex){
            //do nothing
        }

        return $finalPrice;
    }
}
