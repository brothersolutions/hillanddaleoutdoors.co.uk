<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml;

abstract class Configuration extends \Magento\Backend\App\AbstractAction
{

    protected $_posRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \BoostMyShop\PointOfSales\Model\Registry $posRegistry

    ) {
        parent::__construct($context);

        $this->_posRegistry = $posRegistry;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
