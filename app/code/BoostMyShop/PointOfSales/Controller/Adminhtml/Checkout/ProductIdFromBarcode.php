<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout;

class ProductIdFromBarcode extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout
{

    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $barcode = $data['barcode'];

        try
        {
            $barcodeAttribute = $this->_config->getBarcodeAttribute();
            if (!$barcodeAttribute)
                throw new \Exception('No barcode attribute configured');

            $product = $this->_productFactory->create()->loadByAttribute($barcodeAttribute, $barcode);
            if (!$product)
                throw new \Exception('No product matching barcode "'.$barcode.'"');

            $isSellable = $this->getIsSellable($product);
            if(!$this->_config->AllowOutOfStockProductSale() && !$isSellable)
                throw new \Exception(__('Can not add product with barcode %1 as :', $barcode).'<br>'.__('- The product is out of stock').'<br>'.__('- The option to allow the sale of out of stock products is disabled'));

            $result['product_id'] = $product->getId();
            $result['has_options'] = $product->getHasOptions();
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        die(json_encode($result));
    }

    public function getIsSellable($product)
    {
        $websiteId = $this->_posRegistry->getCurrentWebsiteId();

        return $this->_productInformation->getSellable($product, $websiteId);
    }
}
