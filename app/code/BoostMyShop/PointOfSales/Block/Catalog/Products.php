<?php
namespace BoostMyShop\PointOfSales\Block\Catalog;

class Products extends \Magento\Backend\Block\Template
{
    protected $_template = 'Catalog/Products.phtml';

    protected $_products;
    protected $_category;
    protected $_parentCategory;

    protected $_priceHelper;
    protected $_posRegistry = null;
    protected $_categoryFactory = null;
    protected $_productCollectionFactory = null;
    protected $_productInformation = null;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \BoostMyShop\PointOfSales\Model\Registry $posRegistry,
                                \Magento\Framework\Pricing\Helper\Data $priceHelper,
                                \Magento\Catalog\Model\CategoryFactory $categoryFactory,
                                \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
                                \BoostMyShop\PointOfSales\Model\ProductInformation $productInformation,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_posRegistry = $posRegistry;
        $this->_priceHelper = $priceHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productInformation = $productInformation;
        $this->_storeManager = $storeManager;
    }

    public function getProducts()
    {
        if (!$this->_products) {
            $this->_products = $this->_productCollectionFactory->create();
            $this->_products->setStoreId($this->_posRegistry->getCurrentStoreId());
            $this->_products->addFilter('type_id', 'simple');
            $this->_products->addFinalPrice();
            $this->_products->addAttributeToSelect('*');
            $this->_products->setPageSize(100);
            $this->_products->setCurPage(1);

            $categoryId = $this->_posRegistry->getCurrentCategoryId();
            if ($categoryId){
                $this->_products->addCategoriesFilter(array('in' => $categoryId));
            }

            $this->hydrate($this->_products, $this->_posRegistry->getCurrentWebsiteId());
        }

        return $this->_products;
    }

    public function getCategory()
    {
        if (!$this->_category)
        {
            $categoryId = $this->_posRegistry->getCurrentCategoryId();
            $this->_category = $this->_categoryFactory->create()->load($categoryId);
        }
        return $this->_category;
    }

    public function getParentCategory()
    {
        if (!$this->_parentCategory)
        {
            $this->_parentCategory = $this->_categoryFactory->create()->load($this->getCategory()->getparent_id());
        }
        return $this->_parentCategory;
    }

    public function formatPrice($price)
    {
        return $this->_priceHelper->currency($price, true, false);
    }

    public function getProductImage($product)
    {
        return $this->_productInformation->getImage($product);
    }

    public function hydrate(&$collection, $websiteId)
    {
        $previousWebsiteId = $this->_storeManager->getStore()->getWebsiteId();
        $this->_storeManager->getStore()->setWebsiteId($websiteId);
        
        foreach($collection as &$product)
        {
            $qty = $this->_productInformation->getQty($product, $websiteId);
            $product->setQty("".$qty);

            $sellable = $this->_productInformation->getSellable($product, $websiteId);
            $product->setSellable($sellable);

            $finalPrice = $this->_productInformation->getProductFinalPrice($product);
            $product->setPrice($finalPrice);
        }
        
        $this->_storeManager->getStore()->setWebsiteId($previousWebsiteId);
    }

}
