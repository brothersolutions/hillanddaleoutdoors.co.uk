<?php

namespace BoostMyShop\PointOfSales\Model\ResourceModel\Stat;


class BestSellers extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('', '');
    }


    public function getItems($storeId, $from, $to)
    {


        $select = $this->getConnection()
            ->select()
            ->from(['soi' => $this->getTable('sales_order_item')], array(
                    'soi.sku',
                    'soi.name',
                    new \Zend_Db_Expr('SUM(soi.qty_ordered) as qty'),
                    new \Zend_Db_Expr('SUM(soi.base_row_total) as total')
                )
            )
            ->joinLeft(['so' => $this->getTable('sales_order')], 'so.entity_id = soi.order_id', array('store_id'))
            ->where('soi.created_at >= "'.$from.'"')
            ->where('soi.store_id = '.$storeId)
            ->where('soi.created_at <= "'.$to.'"')
            ->where('state NOT IN ("closed", "canceled")')
            ->group('soi.sku')
            ->order('SUM(soi.base_row_total) desc')
            ->limit(10);

        $result = $this->getConnection()->fetchAll($select);

        return $result;
    }


}
