<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Catalog;

class Index extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Catalog
{

    /**
     * @return void
     */
    public function execute()
    {
        $this->setCurrentCategory();

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->addHandle('pointofsales_catalog_index');
        $result = $resultPage->getLayout()->renderElement('content');
        return $this->_resultRawFactory->create()->setContents($result);
    }

    public function setCurrentCategory()
    {
        $categoryId = $this->getRequest()->getParam('category_id');
        if (!$categoryId)
        {
            $storeId = $this->_posRegistry->getCurrentStoreId();
            $store = $this->_storeManager->getStore($storeId);
            $categoryId = $store->getRootCategoryId();
        }

        $this->_posRegistry->setCurrentCategoryId($categoryId);
    }

}
