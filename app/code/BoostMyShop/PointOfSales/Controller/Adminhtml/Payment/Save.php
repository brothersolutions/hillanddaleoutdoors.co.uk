<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Payment;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RawFactory;

class Save extends \Magento\Backend\App\AbstractAction
{
    protected $_orderFactory;
    protected $resultJsonFactory;
    protected $_payment;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \BoostMyShop\PointOfSales\Model\Payment $payment

    ) {
        parent::__construct($context);
        $this->_orderFactory = $orderFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_payment = $payment;
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->_view->loadLayout();

        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @return void
     */
    public function execute()
    {
        try
        {
            $resultJson = $this->resultJsonFactory->create();
            $data = $this->getRequest()->getPostValue();
            if(!isset($data['method']) || !isset($data['amount']) || !isset($data['incrementId']))
                throw new \Exception(__("Please select payment method or enter amount."));

                $order = $this->_orderFactory->create()->loadByIncrementId($data['incrementId']);
                if(!$order->getId())
                    throw new \Exception(__("order not found."));

                $grandTotal = $order->getgrand_total();
                $paidTotal = 0;
                $method = $data['method'];
                $paid = $data['amount'];
                $orderId = $order->getId();
                $incrementId = 'Order #'.$data['incrementId'];

                //save multipayment amount
                $this->_payment->addPayment($method, $paid, $orderId, $incrementId);

                $allPayments = $this->_payment->getCollection()->addOrderFilter($orderId);
                foreach ($allPayments as $payment) {
                    $paidTotal += $payment->getamount();
                }

                $dueTotal = $grandTotal - $paidTotal;

                //update order total
                $order->setbase_total_due($dueTotal)
                    ->settotal_due($dueTotal)
                    ->setbase_total_paid($paidTotal)
                    ->settotal_paid($paidTotal)
                    ->save();

                return $resultJson->setData([
                    'message' => __('Payment updated'),
                    'error' => false
                ]);
        }catch(\Exception $ex){
            return $resultJson->setData([
                'message' =>__('An error occured : %1', $ex->getMessage()),
                'error' => true
            ]);
        }
    }
}
