<?php
namespace BoostMyShop\PointOfSales\Model\Mail;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    public function addAttachment($pdfString, $filename)
    {
        If ($filename == '') {
            $filename="attachment";
        }
        $this->message->createAttachment(
            $pdfString,
            'application/pdf',
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $filename.'.pdf'
        );
        return $this;
    }
}