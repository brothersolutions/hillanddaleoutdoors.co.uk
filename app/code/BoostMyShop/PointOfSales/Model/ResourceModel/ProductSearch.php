<?php

namespace BoostMyShop\PointOfSales\Model\ResourceModel;

class ProductSearch extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    /**
     * Initialize select
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->_joinFields();
        return $this;
    }

    /**
     * Join fields to entity
     *
     * @return $this
     */
    protected function _joinFields()
    {
        $attributes = ['name', 'sku', 'price', 'image', 'base_image'];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $posConfig = $objectManager->create('\BoostMyShop\PointOfSales\Model\Config');
        $barcodeAttribute = $posConfig->getBarcodeAttribute();
        if($barcodeAttribute)
            array_push($attributes, $barcodeAttribute);

        $this->addAttributeToSelect($attributes);

        $posRegistry = $objectManager->create('\BoostMyShop\PointOfSales\Model\Registry');
        $storeId = $posRegistry->getCurrentStoreId();
        $this->setStoreId($storeId);

        return $this;
    }

    public function search($queryString)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $posConfig = $objectManager->create('\BoostMyShop\PointOfSales\Model\Config');
        $barcodeAttribute = $posConfig->getBarcodeAttribute();
        $queryString = trim($queryString);

        if($barcodeAttribute){
            $this->addAttributeToFilter([
                ['attribute' => 'name', 'like' => '%'.$queryString.'%'],
                ['attribute' => 'sku', 'like' => '%'.$queryString.'%'],
                ['attribute' => $barcodeAttribute, 'eq' => $queryString]
            ]);
        } else {
            $this->addAttributeToFilter([
                ['attribute' => 'name', 'like' => '%'.$queryString.'%'],
                ['attribute' => 'sku', 'like' => '%'.$queryString.'%']
            ]);
        }

        $this->addFieldToFilter('type_id', ['in' => ['simple', 'virtual']]);

        $this->setPageSize(50)->setCurPage(1);

        return $this;
    }

}