<?php

namespace BoostMyShop\PointOfSales\Model\ResourceModel;


class Zreport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('', '');
    }

    public function getTotalSales($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('sales_order'), array(new \Zend_Db_Expr('SUM(base_subtotal) + SUM(base_shipping_amount) + SUM(base_discount_amount) + SUM(discount_tax_compensation_amount) as sub_total')))
            ->where('store_id = '.$settings['store_id'])
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
            ->where('state NOT IN ("closed", "canceled")');

        $result = $this->getConnection()->fetchOne($select);
        return $result;
    }

    public function getTotalTax($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('sales_order'), array(new \Zend_Db_Expr('SUM(base_tax_amount) as taxes')))
            ->where('store_id = '.$settings['store_id'])
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
            ->where('state NOT IN ("closed", "canceled")');

        $result = $this->getConnection()->fetchOne($select);
        return $result;
    }

    public function getTaxAmountPerPercent($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from(['soi' => $this->getTable('sales_order_item')], array('tax_percent', new \Zend_Db_Expr('SUM(soi.base_tax_amount) as total')))
            ->joinLeft(['so' => $this->getTable('sales_order')], 'so.entity_id = soi.order_id', array('store_id'))
            ->where('soi.store_id = '.$settings['store_id'])
            ->where('soi.created_at >= "'.$settings['from'].'"')
            ->where('soi.created_at <= "'.$settings['to'].'"')
            ->where('so.state NOT IN ("closed", "canceled")')
            ->where('soi.tax_percent > 0')
            ->group('soi.tax_percent');

        $result = $this->getConnection()->fetchAll($select);
        return $result;
    }

    public function getPaymentsPerMethods($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from(array('o' => $this->getTable('bms_pointofsales_order_payment')), array('method', new \Zend_Db_Expr('SUM(amount) as total')))
            ->joinLeft($this->getTable('sales_order'), 'order_id = entity_id', array('store_id'))
            ->where('store_id = '.$settings['store_id'])
            ->where('o.created_at >= "'.$settings['from'].'"')
            ->where('o.created_at <= "'.$settings['to'].'"')
            ->where('state NOT IN ("closed", "canceled")')
            ->group('method');

        $result = $this->getConnection()->fetchAll($select);
        return $result;
    }

    public function getTransactionsPerMethods($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from(array('o' => $this->getTable('bms_pointofsales_order_payment')), array('method', new \Zend_Db_Expr('count(*) as total')))
            ->joinLeft($this->getTable('sales_order'), 'order_id = entity_id', array('store_id'))
            ->where('store_id = '.$settings['store_id'])
            ->where('o.created_at >= "'.$settings['from'].'"')
            ->where('o.created_at <= "'.$settings['to'].'"')
            ->where('state NOT IN ("closed", "canceled")')
            ->group('method');

        $result = $this->getConnection()->fetchAll($select);
        return $result;
    }

    public function getSales($settings)
    {
        $select = $this->getConnection()
            ->select()
            ->from(array('o' => $this->getTable('sales_order')), array('increment_id','grand_total'))
            ->joinLeft($this->getTable('bms_pointofsales_order_payment'), 'entity_id = order_id', array('method', 'amount'))
            ->where('store_id = '.$settings['store_id'])
            ->where('o.created_at >= "'.$settings['from'].'"')
            ->where('o.created_at <= "'.$settings['to'].'"')
            ->where('state NOT IN ("closed", "canceled")')
            ->order('entity_id asc');

        $result = $this->getConnection()->fetchAll($select);
        return $result;
    }

}
