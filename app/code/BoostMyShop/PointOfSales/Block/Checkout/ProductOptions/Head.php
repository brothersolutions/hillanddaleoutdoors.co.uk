<?php
namespace BoostMyShop\PointOfSales\Block\Checkout\ProductOptions;

class Head extends \BoostMyShop\PointOfSales\Block\Checkout\AbstractCheckout
{
    protected $_template = 'Checkout/ProductOptions/Head.phtml';

    protected $_product = null;

    public function getProduct()
    {
        if ($this->_product == null)
        {
            $productId = $this->_coreRegistry->registry('pos_options_product_id');
            $this->_product = $this->_productFactory->create()->load($productId);
        }
        return $this->_product;
    }

    public function getProductImageUrl()
    {
        return $this->_productInformation->getImage($this->getProduct());
    }

}