define([
    "jquery",
    "mage/translate",
    "prototype"
], function(jQuery, confirm, alert){

    window.AdminPosUi = new Class.create();

    jQuery('#html-body').delegate("[data-action=switch-toggle]", "click", function (e) {
        e.preventDefault();

        checkboxInputId = jQuery(this).data('checkbox-id');
        if (jQuery(this).hasClass("switch-toggle-on"))
        {
            jQuery(this).removeClass("switch-toggle-on");
            jQuery(this).addClass("switch-toggle-off");
            jQuery('#' + checkboxInputId).prop( "checked", false);
        }
        else
        {
            jQuery(this).removeClass("switch-toggle-off");
            jQuery(this).addClass("switch-toggle-on");
            jQuery('#' + checkboxInputId).prop( "checked", true);
        }

    });

    AdminPosUi.prototype = {

        initialize: function(){
            this.popup = null;
            this.numericPopup = null;
            this.paymentpopup = null;
            this.numericPopupCallBack;
            this.disableSound = false;

            this.displayTime();
            setInterval(function(){ objPosUi.displayTime(); }, 60000);
        },

        displayTime: function()
        {
            d = new Date();
            var minutes = d.getMinutes();
            if (minutes.toString().length == 1)
                minutes = '0' + minutes;
            var hours = d.getHours();
            if (hours.toString().length == 1)
                hours = '0' + hours;
            jQuery('#pos_time').html(hours + ":" + minutes);
        },

        //ajax display for screen url
        showScreen: function(divId, title, screenUrl, forceRefresh)
        {

            jQuery('#pos-page-title').html(title);

            //display matching container only
            jQuery('.pos-content').each(function( index ) {
                if ($( this).id == divId)
                    $( this).show();
                else
                    $( this).hide();
            });

            //load screen only the first time
            if ((forceRefresh) || (jQuery('#' + divId).html() == ""))
            {

                var data = {
                    FORM_KEY: window.FORM_KEY
                };

                jQuery.ajax({
                    url: screenUrl,
                    data: data,
                    success: function (resp) {
                        jQuery('#' + divId).html(resp);
                    },
                    failure: function (resp) {
                        jQuery('#' + divId).html(translate("errorOccurred"));
                    }
                });
            }

        },

        showPopup: function(title, screenUrl)
        {
            this.popup = jQuery('<div></div>').modal({
                title: jQuery.mage.__(title),
                type: 'slide',
                closeExisting: false,
                buttons: []
            });
            window.localStorage.setItem('screenUrl',screenUrl);

            var data = {
                FORM_KEY: window.FORM_KEY
            };

            jQuery.ajax({
                url: screenUrl,
                data: data,
                success: function (resp) {
                    objPosUi.popup.html(resp);
                    objPosUi.popup.modal('openModal');
                    objPosUi.playOk();
                }
            });
        },

        showPaymentPopup: function(title, divId)
        {
            this.paymentpopup =
                jQuery('#'+divId).modal({
                    title: title,
                    type: 'slide',
                    closeExisting: true,
                    buttons: []
                });

            this.paymentpopup.modal('openModal');

        },

        savePayment: function(method, dueAmount, paidAmount, url, incrementId)
        {
            if(dueAmount>=paidAmount)
            {
                var data = {method:method,amount:paidAmount,incrementId:incrementId,form_key:window.FORM_KEY}
                jQuery.ajax({
                    url: url,
                    data: data,
                    showLoader: true,
                    method: "POST",
                    dataType: 'json',
                    success: function (resp) {
                        if (resp.error) {
                            objPosUi.playNok();
                            jQuery('.messages').html('<div class="message message-error error">'+ resp.message +'</div>');
                        } else {
                            var formkey = {
                                FORM_KEY: window.FORM_KEY
                            };
                            jQuery.ajax({
                                url: window.localStorage.getItem('screenUrl'),
                                showLoader: true,
                                data: formkey,
                                success: function (resp) {
                                    objPosUi.popup.html(resp);
                                    objPosUi.paymentpopup.modal('closeModal');
                                    objPosUi.playOk();
                                }
                            });
                        }
                    }
                });
            }else {
                jQuery('.messages').html('<div class="message message-error error">Filled paid amount is more than due amount, please check.</div>');
            }
        },

        getPaymentFilter: function(data, jsObjectName)
        {

            var formkey = {
                FORM_KEY: window.FORM_KEY
            };
            jQuery.ajax({
                url: jsObjectName.reload(jsObjectName.addVarToUrl(jsObjectName.filterVar, Base64.encode(data.value))),
                showLoader: true,
                data: formkey,
                success: function (resp) {
                    jQuery(document).find('#orderGrid').html(resp);
                }
            });
        },

        showNumericPopup: function(title, fullValue, callback)
        {
            this.numericPopup = jQuery('#pos_numeric_popup').modal({
                title: title,
                type: 'slide',
                closeExisting: true,
                buttons: []
            });

            jQuery('#pos_numeric_popup_value').html('');
            jQuery('#pos_numeric_custom_button').val(fullValue);

            this.numericPopupCallBack = callback;
            this.numericFullValue = fullValue;

            jQuery('#pos_numeric_popup').modal('openModal');

            objPosUi.playOk();
        },

        applyStatSettings: function(submitUrl)
        {
            var form = jQuery('#frm_stat');
            fields = $('frm_stat').select('input', 'select', 'textarea');
            var data = Form.serializeElements(fields, true);

            jQuery.ajax({
                url: form.attr('action'),
                data: data,
                success: function (resp) {
                    jQuery('#menu-magento-backend-report').click();
                    objPosUi.showScreen('pos-content-report', 'Stats', submitUrl, true);
                }
            });
        },

        numericPopupClick: function(char)
        {
            char = char.toLowerCase();
            switch(char)
            {
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    jQuery('#pos_numeric_popup_value').html(jQuery('#pos_numeric_popup_value').html() + char);
                    break;
                case 'ok':
                    var value = (jQuery('#pos_numeric_popup_value').html());
                    if (!value)
                        value = 0;
                    value = parseFloat(value);
                    if (this.numericPopupCallBack)
                        this.numericPopupCallBack(value);
                    this.numericPopup.modal('closeModal');
                    objPosUi.playOk();
                    break;
                case 'clear':
                    jQuery('#pos_numeric_popup_value').html('');
                    break;
                case 'pos_numeric_custom_button':
                    jQuery('#pos_numeric_popup_value').html(this.numericFullValue);
                    this.numericPopupClick('ok');
                    break;
            }
        },

        hidePopup: function()
        {
            this.popup.modal('closeModal');
        },

        initMultiplePayment: function(currencyFormat, grandTotal)
        {
            this.currency_format = currencyFormat;
            this.grandTotal = grandTotal;

            jQuery('#multiple_total_due').html(this.currencyFormat(this.grandTotal));

            this.updateMultipleTotals();
        },

        updateMultipleTotals: function()
        {
            jQuery('#multiple_total_paid').html(this.currencyFormat(this.getTotalPaid()));
            jQuery('.discount-currency').html(this.currency_format.replace('%s',''));
            var balance = this.getTotalDue();
            var color = 'black';
            if (balance == 0)
                color = 'green';
            else
                color = 'red';

            jQuery('#multiple_total_balance').html('<font color="' + color + '">' + this.currencyFormat(balance) + '</font>');
        },

        getTotalPaid: function()
        {
            var totalPaid = 0;
            if ($('payment_form_multiple_payment'))
            {
                fields = $('payment_form_multiple_payment').select('input');
                jQuery.each( fields, function( key, value ) {
                    if (value.value)
                        totalPaid += parseFloat(value.value);
                });
            }
            return parseFloat(totalPaid).toFixed(2);
        },

        getTotalDue: function()
        {
            return parseFloat(this.grandTotal - this.getTotalPaid()).toFixed(2);
        },

        currencyFormat: function(value)
        {
            value = parseFloat(value).toFixed(2);
            value = this.currency_format.replace('%s', value);
            return value;
        },

        playOk: function()
        {
            if (!objPosUi.disableSound)
                jQuery("#audio_ok").get(0).play();
        },

        playNok: function ()
        {
            if (!objPosUi.disableSound)
                jQuery("#audio_nok").get(0).play();
        }
    };

});
