<?php


namespace BoostMyShop\PointOfSales\Block\System\Config;


class FlushCurrentUserSettings extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_template = 'BoostMyShop_PointOfSales::System/Config/System/FlushCurrentUserSettings.phtml';

    /**
     * @var \Magento\MediaStorage\Model\File\Storage
     */
    protected $_fileStorage;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\MediaStorage\Model\File\Storage $fileStorage
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\MediaStorage\Model\File\Storage $fileStorage,
        array $data = []
    ) {
        $this->_fileStorage = $fileStorage;
        parent::__construct($context, $data);
    }

    /**
     * Remove scope label
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Generate synchronize button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'flush_current_user_settings_button',
                'label' => __('Flush'),
                'onclick' => "setLocation('".$this->getFlushUrl()."')"
            ]
        );

        return $button->toHtml();
    }

    public function getFlushUrl()
    {
        return $this->getUrl('pointofsales/configuration/flush');
    }

}
