<?php

namespace BoostMyShop\PointOfSales\Plugin\CatalogInventory\Model;

class StockStateProvider{

    protected $request = null;
    protected $_config = null;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \BoostMyShop\PointOfSales\Model\Config $config
    ){
        $this->request = $request;
        $this->_config = $config;
    }

    public function afterCheckQuoteItemQty(\Magento\CatalogInventory\Model\StockStateProvider $subject, $result)
    {
        $moduleName = $this->request->getModuleName();

        //If OOS option enabled in POS, remove stock state messages to allow order creation for OOS products
        if(isset($moduleName) && $moduleName == 'pointofsales'){
            if($this->_config->AllowOutOfStockProductSale()){
                $result->setHasError(false);
                $result->setMessage('');
                $result->setItemUseOldQty(false);
            }
        }

        return $result;
    }
}