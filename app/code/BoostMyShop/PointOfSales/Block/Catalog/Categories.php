<?php
namespace BoostMyShop\PointOfSales\Block\Catalog;

class Categories extends \Magento\Backend\Block\Template
{
    protected $_template = 'Catalog/Categories.phtml';

    protected $_categories;
    protected $_category;
    protected $_parentCategory;

    protected $_posRegistry = null;
    protected $_categoryFactory = null;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \BoostMyShop\PointOfSales\Model\Registry $posRegistry,
                                \Magento\Catalog\Model\CategoryFactory $categoryFactory,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_posRegistry = $posRegistry;
        $this->_categoryFactory = $categoryFactory;
    }

    public function getCategory()
    {
        if (!$this->_category)
        {
            $categoryId = $this->_posRegistry->getCurrentCategoryId();
            $this->_category = $this->_categoryFactory->create()->load($categoryId);
        }
        return $this->_category;
    }

    public function getParentCategory()
    {
        if (!$this->_parentCategory)
        {
            $this->_parentCategory = $this->_categoryFactory->create()->load($this->getCategory()->getparent_id());
        }
        return $this->_parentCategory;
    }

    public function getCategories()
    {
        if (!$this->_categories)
        {
            $this->_categories = $this->_categoryFactory->create()->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('is_active', 1)
                            ->setOrder('position', 'ASC')
                            ->addFieldToFilter('parent_id', $this->getCategory()->getId());
        }

        return $this->_categories;
    }

    public function getCategoryUrl($category)
    {
        return $this->getUrl('pointofsales/catalog/index', ['category_id' => $category->getId()]);
    }

    public function getParentCategoryUrl()
    {
        return $this->getUrl('pointofsales/catalog/index', ['category_id' => $this->getCategory()->getparent_id()]);
    }

}