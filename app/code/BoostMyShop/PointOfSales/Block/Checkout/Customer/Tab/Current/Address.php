<?php

namespace BoostMyShop\PointOfSales\Block\Checkout\Customer\Tab\Current;

class Address extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $_LocaleLists;
    protected $_countryLists;
    protected $_regionFactory;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        array $data = []
    ) {
        $this->_LocaleLists = $localeLists;
        $this->_countryLists = $countryCollectionFactory;
        $this->_regionFactory = $regionFactory;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {

        $form = $this->_formFactory->create();
        $baseFieldset = $form->addFieldset('fieldset_customer_address', ['legend' => __('Address'), 'class' => 'admin__fieldset']);

        $regionCollection = $this->_regionFactory->create()->getCollection()->addCountryFilter(
            $baseFieldset['customer_address_country_id']
        );

        $regions = $regionCollection->toOptionArray();

        $baseFieldset->addField(
            'customer_address_firstname',
            'text',
            [
                'name' => 'firstname',
                'label' => __('Firstname'),
                'id' => 'firstname',
                'title' => __('Firstname')
            ]
        );

        $baseFieldset->addField(
            'customer_address_lastname',
            'text',
            [
                'name' => 'lastname',
                'label' => __('Lastname'),
                'id' => 'lastname',
                'title' => __('Lastname')
            ]
        );

        $baseFieldset->addField(
            'customer_address_company',
            'text',
            [
                'name' => 'company',
                'label' => __('Company'),
                'id' => 'company',
                'title' => __('Company')
            ]
        );

        $baseFieldset->addField(
            'customer_address_street0',
            'text',
            [
                'name' => 'street0',
                'label' => __('Street'),
                'id' => 'street0',
                'title' => __('Street')
            ]
        );

        $baseFieldset->addField(
            'customer_address_street1',
            'text',
            [
                'name' => 'street1',
                'label' => __('Street'),
                'id' => 'street1',
                'title' => __('Street')
            ]
        );

        $baseFieldset->addField(
            'customer_address_city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'id' => 'city',
                'title' => __('City')
            ]
        );

        $country = $baseFieldset->addField(
            'customer_address_country_id',
            'select',
            [
                'name' => 'country_id',
                'label' => __('Country'),
                'title' => __('Country'),
                'required' => true,
                'values' => $this->_countryLists->create()->toOptionArray(),
                'class' => 'select'
            ]
        );

        $baseFieldset->addField(
            'customer_address_region_id',
            'select',
            [
                'name' => 'region_id',
                'label' => __('Region'),
                'title' => __('Region'),
                'values' =>  $regions
            ]
        );

        $baseFieldset->addField(
            'customer_address_postcode',
            'text',
            [
                'name' => 'postcode',
                'label' => __('Postcode'),
                'id' => 'postcode',
                'title' => __('Postcode')
            ]
        );

        $baseFieldset->addField(
            'customer_address_telephone',
            'text',
            [
                'name' => 'telephone',
                'label' => __('Telephone'),
                'id' => 'telephone',
                'title' => __('Telephone')
            ]
        );

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock('Magento\Framework\View\Element\Template')->setTemplate('BoostMyShop_PointOfSales::Checkout/RegionUpdater.phtml')
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

}
