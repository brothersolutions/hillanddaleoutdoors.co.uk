<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout;

class CustomerInformation extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout
{

    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $result = [];

        try
        {
            $customerId = isset($data['customer_id']) ? $data['customer_id'] : 'guest';

            switch($customerId)
            {
                case 'guest':
                    $result = $this->getGuestInformation();
                    $result['address'] = $this->getGuestDefaultAddress();
                    break;

                default:
                    $result = $this->getCustomerInformation($customerId);

                    // Customer loaded have no address set in Magento : apply Guest address fields depending of configuration
                    if (!isset($result['address'])){
                        $customerFirstName = ($result['customer']['firstname']) ? $result['customer']['firstname'] : '';
                        $customerLastName = ($result['customer']['lastname']) ? $result['customer']['lastname'] : '';
                        $result['address'] = $this->getCustomerDefaultAddress($customerFirstName, $customerLastName);
                    }

                    break;
            }

            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        die(json_encode($result));
    }

    //----------------------------------------------- Customer data -----------------------------------------------//

    protected function getCustomerInformation($customerId)
    {
        if (!$customerId)
            throw new \Exception('This customer does not exist');

        $customer = $this->_customerFactory->create()->load($customerId);

        if (!$customer)
            throw new \Exception('Customer load has failed');

        $result = [];

        $result['customer'] = $customer->getData();
        $result['customer']['mode'] = 'customer';
        $result['default_country_id'] = $this->_config->getGlobalSetting('general/country/default');

        if ($customer->getDefaultBillingAddress())
            $result['address'] = $customer->getDefaultBillingAddress()->getData();
        elseif ($customer->getDefaultShippingAddress())
            $result['address'] = $customer->getDefaultShippingAddress()->getData();

        return $result;
    }

    protected function getCustomerDefaultAddress($firstName, $lastName)
    {
        $address = [];

        $address['firstname'] = ($firstName) ? $firstName : (($this->_config->getGuestField('firstname')) ? $this->_config->getGuestField('firstname') : '');
        $address['lastname'] = ($lastName) ? $lastName : (($this->_config->getGuestField('lastname')) ? $this->_config->getGuestField('lastname') : '');
        $address['street'] = ($this->_config->getGuestField('street')) ? $this->_config->getGuestField('street') : '';
        $address['company'] = '';
        $address['country_id'] = ($this->_config->getGuestField('country_id')) ? $this->_config->getGuestField('country_id') : $this->_config->getGlobalSetting('general/country/default');
        $address['region_id'] = ($this->_config->getGuestField('region_id')) ? $this->_config->getGuestField('region_id') : '';
        $address['city'] = ($this->_config->getGuestField('city')) ? $this->_config->getGuestField('city') : '';
        $address['postcode'] = ($this->_config->getGuestField('postcode')) ? $this->_config->getGuestField('postcode') : '';
        $address['telephone'] = ($this->_config->getGuestField('telephone')) ? $this->_config->getGuestField('telephone') : '';

        return $address;
    }

    //----------------------------------------------- Guest data -----------------------------------------------//

    protected function getGuestInformation()
    {
        $result = [];

        $result['customer'] = [];
        $result['customer']['mode'] = 'guest';
        $result['customer']['entity_id'] = '';
        $result['customer']['email'] = ($this->_config->getGuestField('email')) ? $this->_config->getGuestField('email') : "";
        $result['customer']['taxvat'] = '';
        $result['customer']['group_id'] = ($this->_config->getGuestField('group')) ? $this->_config->getGuestField('group') : 0;
        $result['customer']['website_id'] = '';

        return $result;
    }

    protected function getGuestDefaultAddress()
    {
        $address = [];

        $address['firstname'] = ($this->_config->getGuestField('firstname')) ? $this->_config->getGuestField('firstname') : '';
        $address['lastname'] = ($this->_config->getGuestField('lastname')) ? $this->_config->getGuestField('lastname') : '';
        $address['street'] = ($this->_config->getGuestField('street')) ? $this->_config->getGuestField('street') : '';
        $address['company'] = '';
        $address['country_id'] = ($this->_config->getGuestField('country_id')) ? $this->_config->getGuestField('country_id') : $this->_config->getGlobalSetting('general/country/default');
        $address['region_id'] = ($this->_config->getGuestField('region_id')) ? $this->_config->getGuestField('region_id') : '';
        $address['city'] = ($this->_config->getGuestField('city')) ? $this->_config->getGuestField('city') : '';
        $address['postcode'] = ($this->_config->getGuestField('postcode')) ? $this->_config->getGuestField('postcode') : '';
        $address['telephone'] = ($this->_config->getGuestField('telephone')) ? $this->_config->getGuestField('telephone') : '';

        return $address;
    }
}
