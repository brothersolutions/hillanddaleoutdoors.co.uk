<?php

namespace Hiddentechies\Googlecustomerreview\Model\Config\Source;

/**
 * Display mode
 *
 */
class Badgelang implements \Magento\Framework\Option\ArrayInterface {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'af', 'label' => __('af')],
            ['value' => 'ar-AE', 'label' => __('ar-AE')],
            ['value' => 'cs', 'label' => __('cs')],
            ['value' => 'da', 'label' => __('da')],
            ['value' => 'de', 'label' => __('de')],
            ['value' => 'en_AU', 'label' => __('en_AU')],
            ['value' => 'en_GB', 'label' => __('en_GB')],
            ['value' => 'en_US', 'label' => __('en_US')],
            ['value' => 'es', 'label' => __('es')],
            ['value' => 'es-419', 'label' => __('es-419')],
            ['value' => 'fil', 'label' => __('fil')],
            ['value' => 'fr', 'label' => __('fr')],
            ['value' => 'ga', 'label' => __('ga')],
            ['value' => 'id', 'label' => __('id')],
            ['value' => 'it', 'label' => __('it')],
            ['value' => 'ja', 'label' => __('ja')],
            ['value' => 'ms', 'label' => __('ms')],
            ['value' => 'nl', 'label' => __('nl')],
            ['value' => 'no', 'label' => __('no')],
            ['value' => 'pl', 'label' => __('pl')],
            ['value' => 'pt_BR', 'label' => __('pt_BR')],
            ['value' => 'pt_PT', 'label' => __('pt_PT')],
            ['value' => 'ru', 'label' => __('ru')],
            ['value' => 'sv', 'label' => __('sv')],
            ['value' => 'tr', 'label' => __('tr')],
            ['value' => 'zh-CN', 'label' => __('zh-CN')],
            ['value' => 'zh-TW', 'label' => __('zh-TW')],
        ];
    }
    

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

}
