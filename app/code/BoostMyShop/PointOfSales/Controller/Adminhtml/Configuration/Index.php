<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Configuration;

class Index extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Configuration
{

    public function execute()
    {
        $this->_redirect('adminhtml/system_config/edit', ['section' => 'pointofsales']);
    }

}
