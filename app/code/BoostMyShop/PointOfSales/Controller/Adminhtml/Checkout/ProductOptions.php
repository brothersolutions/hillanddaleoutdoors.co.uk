<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout;

class ProductOptions extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout
{

    /**
     * @return void
     */
    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $this->_coreRegistry->register('pos_options_product_id', $productId);

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->addHandle('pointofsales_checkout_productoptions');
        $result = $resultPage->getLayout()->renderElement('content');
        return $this->_resultRawFactory->create()->setContents($result);
    }
}
