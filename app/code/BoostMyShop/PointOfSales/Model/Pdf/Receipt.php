<?php

namespace BoostMyShop\PointOfSales\Model\Pdf;


class Receipt extends \Magento\Sales\Model\Order\Pdf\AbstractPdf
{
    protected $_storeManager;
    protected $_localeResolver;
    protected $_orderFactory;
    protected $_config;
    protected $_managerFactory;
    protected $_paymentCollectionFactory;

    protected $_sectionMargin = 20;
    protected $_margin = 10;
    protected $_maxImageHeight = 80;
    protected $_logo;

    protected $_currentOrder;

    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \BoostMyShop\PointOfSales\Model\Config $config,
        \BoostMyShop\PointOfSales\Model\Order\ManagerFactory $managerFactory,
        \BoostMyShop\PointOfSales\Model\ResourceModel\Payment\CollectionFactory $paymentCollectionFactory,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_localeResolver = $localeResolver;
        $this->_orderFactory = $orderFactory;
        $this->_config = $config;
        $this->_managerFactory = $managerFactory;
        $this->_paymentCollectionFactory = $paymentCollectionFactory;

        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $data
        );

    }


    /**
     * Return PDF document
     *
     * @param array|Collection
     * @return \Zend_Pdf
     */
    public function getPdf($orders = [])
    {
        $this->_beforeGetPdf();

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($orders as $order) {

            $this->_currentOrder = $order;

            if ($order->getStoreId()) {
                $this->_localeResolver->emulate($order->getstore_id());
                $this->_storeManager->setCurrentStore($order->getstore_id());
            }
            $page = $this->newPage();

            $this->_drawOrderInformation($this->_getPdf()->pages[$this->getLastPage()], $order);
            $this->_drawProductHeader($this->_getPdf()->pages[$this->getLastPage()]);
            foreach ($order->getAllItems() as $item) {
                $this->_drawProduct($item, $this->_getPdf()->pages[$this->getLastPage()], $order);
                $page = end($pdf->pages);
            }
            $this->y -= $this->_sectionMargin;
			
            $this->_drawTotals($this->_getPdf()->pages[$this->getLastPage()], $order);
            $this->_drawPayments($this->_getPdf()->pages[$this->getLastPage()], $order);
            $this->_drawFooter($this->_getPdf()->pages[$this->getLastPage()]);

            if ($order->getStoreId()) {
                $this->_localeResolver->revert();
            }
        }
        $this->_afterGetPdf();

        return $pdf;
    }

    public function getWidth()
    {
        $storeId = $this->_storeManager->getStore()->getId();
        return ((($this->_config->getReceiptWidth($storeId)) / 2.54) * 595) / 8.26;
    }

    public function getHeight()
    {
        $height = 0;
        $storeId = $this->_storeManager->getStore()->getId();

        //logo
        $logo = $this->getLogo($storeId);
        if ($logo) {
            $logoSizes = $this->getLogoInfo($logo);
            $height += $logoSizes['height'];
        }
        $height += $this->_sectionMargin;

        //header
        $headerText = $this->_config->getReceiptHeaderText($storeId);
        $headerText = $this->splitTextToSize($headerText, $this->getFontRegular(), 24, $this->getWidth() - 20);
        $height += count($headerText) * 15;
        $height += $this->_sectionMargin;

        //order information
        $height += 3 * 15;
        $height += $this->_sectionMargin;

        //products
        $height += (count($this->_currentOrder->getAllItems()) + 1) * 15;
        $height += $this->_sectionMargin;

        //totals
        $height += 4 * 15;
        $height += $this->_sectionMargin;

        //payments
        $payments = $this->_paymentCollectionFactory->create()->addOrderFilter($this->_currentOrder->getId());
        $height += 15 + 15 * count($payments);
        $height += $this->_sectionMargin;

        //footer
        $footerText = $this->_config->getReceiptFooterText($storeId);
        $footerText = $this->splitTextToSize($footerText, $this->getFontRegular(), 12, $this->getWidth() - 20);
        $height += count($footerText) * 15;
        $height += $this->_sectionMargin;

        return $height;
    }

    public function newPage(array $settings = [])
    {
        $size = $this->getWidth(). ':' . $this->getHeight().':';
        $page = $this->_getPdf()->newPage($size);
        $this->_getPdf()->pages[] = $page;
        $this->y = $this->getHeight();
        $this->_addLogo($page);
        $this->_drawHeader($page);
        return $page;
    }

	public function getLastPage()
	{
		return count($this->_getPdf()->pages) - 1;
	}
	
    /**
     * Add Logo
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _addLogo(\Zend_Pdf_Page $page)
    {
        $image = $this->getLogo();

        if ($image)
        {
            $imageInfo = $this->getLogoInfo($image);
            $page->drawImage($image, ($this->getWidth()/2) - ($imageInfo['width']/2), $this->y - $imageInfo['height'], ($this->getWidth()/2) + ($imageInfo['width']/2), $this->y);
            $this->y -= $imageInfo['height'];
        }
    }

    protected function getLogo()
    {
        if($this->_logo == null) {
            $storeId = $this->_storeManager->getStore()->getId();
            $path = $this->_config->getLogo($storeId);

            $logoPath = $this->_rootDirectory->getAbsolutePath() . 'pub/media/pos/logo/' . $path;
            if (file_exists($logoPath))
                $this->_logo = \Zend_Pdf_Image::imageWithPath($logoPath);

        }
        return $this->_logo;
    }

    protected function getLogoInfo($image)
    {
        $width = $this->getWidth();
        $maxWidth = $width - 2 * $this->_margin;
        $imageWidth = $image->getPixelWidth();
        $imageHeight = $image->getPixelHeight();

        $imageInfo = array();

        if(($imageWidth - $maxWidth) > ($imageHeight - $this->_maxImageHeight)) {
            $ratio = $imageWidth / $maxWidth;
        }else{
            $ratio = $imageHeight / $this->_maxImageHeight;
        }

        $imageInfo['height'] = $imageHeight / $ratio;
        $imageInfo['width'] = $imageWidth / $ratio;

        return $imageInfo;
    }

    /**
     * Draw header for item table
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(\Zend_Pdf_Page $page)
    {
        $this->_setFontBold($page, 12);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $storeId = $this->_storeManager->getStore()->getId();

        $headerText = $this->_config->getReceiptHeaderText($storeId);
        $headerText = explode("\n", str_replace("\r","",$headerText));
        foreach($headerText as $line) {
            $txtWidth = $this->widthForStringUsingFontSize($line, $this->getFontRegular(), 12);
            $x = ($this->getWidth() - $txtWidth) / 2;
            $page->drawText($line, $x, $this->y - 20, 'UTF-8');
            $this->y -= 15;
        }

        $this->y -= $this->_sectionMargin;

    }

    protected function _drawOrderInformation(\Zend_Pdf_Page $page, $order)
    {
        $lines = [];
        $lines[] = __('Order #').$order->getincrement_id();
        $lines[] = __('Date : ').$order->getCreatedAtFormatted(2);
        $lines[] = __('Seller : ').$this->_managerFactory->create()->loadByOrderId($order->getId())->getUser()->getName();

        $this->setFontRegular($page, 12);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        foreach($lines as $line) {
            $page->drawText($line, 10, $this->y - 20, 'UTF-8');
            $this->y -= 15;
        }

        $this->y -= $this->_sectionMargin;
    }

    protected function _drawProductHeader($page)
    {
        $this->_setFontBold($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        $lines[0][] = ['text' => __('Qty'), 'feed' => 10, 'align' => 'left'];
        $lines[0][] = ['text' => __('Product'), 'feed' => 30, 'align' => 'left'];
        $lines[0][] = ['text' => __('Price'), 'feed' => $this->getWidth() - 20, 'align' => 'right'];
        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->drawLine(5, $this->y, $this->getWidth() - 10, $this->y);
        $this->y -= 15;


    }

    /**
     * @param $item
     * @param $page
     * @param $order
     */
    protected function _drawProduct($item, $page, $order)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        //columns headers
        $lines[0][] = ['text' => (int)$item->getqty_ordered(), 'feed' => 10, 'align' => 'left'];
        $itemPrice = $item->getprice_incl_tax() ? : $item->getPrice();
        $lines[0][] = ['text' => $order->formatPriceTxt($itemPrice), 'feed' => $this->getWidth() - 20, 'align' => 'right'];
        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        $this->y += 5;

        $nameLines = $this->splitTextToSize($item->getName().' ('.$item->getSku().')', $page->getFont(), 10, 160);
        $commentLines = $this->splitTextToSize($item->getpos_comment(), $page->getFont(), 10, 160);
        foreach($nameLines as $nameLine) {
            $page->drawText($nameLine, 30, $this->y, 'UTF-8');
            $this->y -= 10;
        }
        $this->y -= 5;
        foreach($commentLines as $commentLine) {
            $page->drawText($commentLine, 30, $this->y, 'UTF-8');
            $this->y -= 10;
        }
        $this->y -= 10;

    }

    protected function _drawTotals($page, $order)
    {
        $totals = [];
        $totals['Subtotal'] = $order->getSubtotal();
        $totals['Discount'] = $order->getDiscountAmount();
        $totals['Shipping'] = $order->getshipping_amount();
        $totals['Taxes'] = $order->gettax_amount();
        $totals['Total'] = $order->getGrandTotal();

        $lines = [];
        foreach($totals as $k => $v)
        {
            if ($v == 0)
                continue;
            $line = [];
            $line[] = ['text' => __("$k"), 'feed' => $this->getWidth() - 140, 'align' => 'left', 'font' => 'LinLibertine_Bd-2.8.1.ttf', 'font_size' => 12];
            $line[] = ['text' => $order->formatPriceTxt($v), 'feed' => $this->getWidth() - 20, 'align' => 'right', 'font' => 'LinLibertine_Bd-2.8.1.ttf', 'font_size' => 12];
            $lines[] = $line;
        }
        $lineBlock = ['lines' => $lines, 'height' => 15];
        $this->drawLineBlocks($page, [$lineBlock]);
        $this->y -= $this->_sectionMargin;
    }

    protected function _drawPayments($page, $order)
    {
        $this->_setFontRegular($page, 12);

        $page->drawLine(5, $this->y, $this->getWidth() - 10, $this->y);
        $this->y -= 15;

        $payments = $this->_paymentCollectionFactory->create()->addOrderFilter($order->getId());
        $lines = [];
        foreach($payments as $payment)
        {
            $line = [];
            $line[] = ['text' => ucfirst($payment->getMethod()), 'feed' => 10, 'align' => 'left', 'font' => 'LinLibertine_Re-4.4.1.ttf', 'font_size' => 12];
            $line[] = ['text' => $order->formatPriceTxt($payment->getAmount()), 'feed' => $this->getWidth() - 20, 'align' => 'right', 'font' => 'LinLibertine_Re-4.4.1.ttf', 'font_size' => 12];
            $lines[] = $line;

        }
        $lineBlock = ['lines' => $lines, 'height' => 15];
        $this->drawLineBlocks($page, [$lineBlock]);
    }

    public function _getFooterLength($footerText)
    {
        if (!empty($footerText)) {
            return strlen($footerText);
        } else {
            return 0;
        }
    }

    public function _getRemainingPageHeight($footerText)
    {
        $usedHeight = $this->y - $this->_getFooterLength($footerText);
        if($usedHeight > 0)
            $remainingPageHeight = $this->getHeight() - $usedHeight;
        else
            $remainingPageHeight = 0;

        return $remainingPageHeight;
    }

    public function _footerFonts($page)
    {
        $this->_setFontRegular($page, 12);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.3));
        return $page;
    }
	
    protected function _drawFooter(\Zend_Pdf_Page $page)
    {
        $this->_footerFonts($page);
        $storeId = $this->_storeManager->getStore()->getId();

        $page->drawLine(5, $this->y, $this->getWidth() - 10, $this->y);

        $footerText = $this->_config->getReceiptFooterText($storeId);
        $footerLines = explode("\n", str_replace("\r", "", $footerText));
        if($this->_getRemainingPageHeight($footerText) <= 0){
            $page = $this->newPage();
            $this->_footerFonts($page);
            foreach($footerLines as $line) {
                $page->drawText($line, 10, $this->y - 20, 'UTF-8');
                $this->y -= 15;
            }
        }else{
            foreach($footerLines as $line) {
                $page->drawText($line, 10, $this->y - 20, 'UTF-8');
                $this->y -= 15;
            }
        }
        
    }

    public function getPdfObject()
    {
        return $this->_getPdf();
    }

    public function setFontBold($page, $size)
    {
        $this->_setFontBold($page, $size);
        return $this;
    }

    public function setFontRegular($page, $size)
    {
        $this->_setFontRegular($page, $size);
        return $this;
    }

    protected function getFontRegular()
    {
        $font = \Zend_Pdf_Font::fontWithPath(
            $this->_rootDirectory->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertine_Re-4.4.1.ttf')
        );
        return $font;
    }

    protected function splitTextToSize($text, $font, $fontSize, $maxWidth)
    {
        $textSize = $this->widthForStringUsingFontSize($text, $font, $fontSize);

        $lines = [];
        if ($textSize > $maxWidth)
        {
            $words = explode(' ', $text);
            $currentLine = '';

            foreach($words as $word)
            {
                if ($this->widthForStringUsingFontSize($currentLine.$word, $font, $fontSize) < $maxWidth)
                    $currentLine .= $word.' ';
                else
                {
                    $lines[] = $currentLine;
                    $currentLine = $word.' ';
                }
            }

            if($currentLine != ''){
                $lines[] = $currentLine;
            }

        }
        else
            $lines[] = $text;

        return $lines;
    }

}
