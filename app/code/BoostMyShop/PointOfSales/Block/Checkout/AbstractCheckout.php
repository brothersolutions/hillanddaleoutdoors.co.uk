<?php
namespace BoostMyShop\PointOfSales\Block\Checkout;

class AbstractCheckout extends \Magento\Backend\Block\Template
{
    protected $_coreRegistry = null;
    protected $_productInformation = null;
    protected $_productFactory = null;
    protected $_config = null;
    protected $_productOptionRepository = null;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magento\Framework\Registry $registry,
                                \BoostMyShop\PointOfSales\Model\ProductInformation $productInformation,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                \BoostMyShop\PointOfSales\Model\Config $config,
                                \Magento\Catalog\Api\ProductCustomOptionRepositoryInterface $productOptionRepository,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_coreRegistry = $registry;
        $this->_productInformation = $productInformation;
        $this->_productFactory = $productFactory;
        $this->_config = $config;
        $this->_productOptionRepository = $productOptionRepository;
    }

}