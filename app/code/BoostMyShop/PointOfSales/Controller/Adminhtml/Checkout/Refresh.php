<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout;

class Refresh extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout
{
    protected $_lastPerfLogTimestamp = null;

    /**
     * @return void
     */
    public function execute()
    {
        $this->addPerfLog('start');

        $data = $this->getRequest()->getPostValue();

        $quoteId = (isset($data['quote_id']) ? $data['quote_id'] : null);
        $createOrder = (isset($data['create_order']) && ($data['create_order'] == 'true') ? true : false);
        $result = ['quote_id' => $quoteId];

        try
        {
            $this->_storeManager->setCurrentStore($this->_registry->getCurrentStoreId());

            //init new quote or load existing one
            if (!$quoteId)
            {
                $storeId = $this->_registry->getCurrentStoreId();
                $store = $this->_storeFactory->create()->load($storeId);
                $this->_quoteHelper->initQuote($store, '');
                $this->setGuestCustomer();
                $this->addPerfLog('after create quote');
            }
            else
            {
                $this->_quoteHelper->load($quoteId);
                $this->addPerfLog('after load quote');
            }

            //customer
            if (array_key_exists('customer', $data)) {
                if (isset($data['customer'])) {
                    switch ($data['customer']['mode']) {
                        case 'guest':
                            $this->setGuestCustomer();
                            break;
                        case 'customer':
                            $this->setCustomerData($data['customer'], $data['address']);
                            break;
                    }
                }
            }
            $this->addPerfLog('after set customer data');

            //products
            if (isset($data['products'])) {
                foreach ($data['products'] as $productId => $productData) {
                    $qty = $productData['qty'];
                    $shipLater = $productData['ship_later'];
                    $comment = (isset($productData['comment']) ? $productData['comment'] : '');
                    $discount = (isset($productData['discount']) ? $productData['discount'] : '');
                    $customPrice = (isset($productData['custom_price']) ? $productData['custom_price'] : '');
                    $options = (isset($productData['options']) ? $productData['options'] : []);
                    $this->_quoteHelper->addOrUpdateProduct($productId, $qty, $customPrice, $shipLater, $options, $comment, $discount);
                }
            }
            $this->addPerfLog('after set products');

            //coupon
            if (isset($data['coupon_code']) && $data['coupon_code'])
                $this->_quoteHelper->applyCoupon($data['coupon_code']);
            else
                $this->_quoteHelper->applyCoupon('');
            $this->addPerfLog('after set coupon');

            //global discount
            if (isset($data['global_discount']) && !empty($data['global_discount']) && isset($data['selected_type']))
            {
                $couponCode = $this->_salesRule->createCoupon($data['global_discount'], $data['selected_type'], $this->_quoteHelper->getCurrentQuoteId(), $this->_registry->getCurrentWebsiteId());
                $this->_quoteHelper->applyCoupon($couponCode);
            }

            //shipping method
            $shippingMethod = $this->_config->getDefaultShippingMethod($this->_registry->getCurrentWebsiteId());
            if (isset($data['shipping_method']))
                $shippingMethod = $data['shipping_method']['method'];
            if ($shippingMethod)
                $this->_quoteHelper->setShippingMethod($shippingMethod);
            $this->addPerfLog('after set shipping');

            //payment method
            $paymentMethod = $this->_config->getDefaultPaymentMethod($this->_registry->getCurrentWebsiteId());
            $paymentData = [];
            if (isset($data['payment_method']))
                $paymentMethod = $data['payment_method'];
            if (isset($data['payment']))
                $paymentData = $data['payment'];
            if ($paymentMethod)
                $this->_quoteHelper->setPaymentMethod($paymentMethod, $paymentData);
            $this->addPerfLog('after set payment');


            //order creation
            if ($createOrder)
            {
                $order = $this->_quoteHelper->placeOrder();
                if (!$order)
                    throw new \Exception('Order creation failed');
                try
                {
                    $order = $this->_orderFactory->create()->load($order->getId());

                    //Create invoice
                    $createInvoice = (isset($data['create_invoice']) && ($data['create_invoice'] == 'true') ? true : false);
                    if ($createInvoice)
                        $this->_invoiceHelper->createInvoice($order);

                    //Create shipment
                    $order = $this->_orderFactory->create()->load($order->getId()); //prevent issue with advancedStock module that doesn't update qty to ship at order item level if we don't reload the order before creating the shipment

                    $createShipment = (isset($data['create_shipment']) && ($data['create_shipment'] == 'true') ? true : false);
                    if ($createShipment)
                    {
                        $shippable = [];
                        $notShippable = [];
                        $productsIdsToShip = $this->_quoteHelper->getProductIdsToShip();
                        foreach($order->getAllItems() as $item)
                        {
                            //if product don't have to be shipped
                            if(array_search($item->getproduct_id(), $productsIdsToShip) === false)
                                continue;

                            $product = $this->_productFactory->create()->load($item->getproduct_id());
                            if($this->_config->isAdvancedStockEnabled())
                            {
                                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                                $extendedOrderItemFactory = $objectManager->create('\BoostMyShop\AdvancedStock\Model\ExtendedSalesFlatOrderItemFactory');
                                $extendedOrderItem = $extendedOrderItemFactory->create()->loadByItemId($item->getId());
                                if($extendedOrderItem->getesfoi_qty_reserved() == $extendedOrderItem->getesfoi_qty_to_ship())
                                    $shippable[] = $product->getId();
                                else
                                    $notShippable[] = $product->getName().' ('.__('%1 qty to ship, %2 qty reserved', (int) $extendedOrderItem->getesfoi_qty_to_ship(), (int) $extendedOrderItem->getesfoi_qty_reserved()).')';
                            }
                            else
                            {
                                $availableQty = $this->_productInformation->getQty($product, $this->_registry->getCurrentWebsiteId());
                                if ($availableQty >= $item->getqty_ordered())
                                    $shippable[] = $product->getId();
                                else
                                    $notShippable[] = $product->getName() . ' (' . __('%1 qty to ship, %2 qty available', (int)$item->getqty_ordered(), (int)$availableQty) . ')';
                            }
                        }

                        if(!$this->_config->AllowOutOfStockProductSale() && count($notShippable) > 0)
                            throw new \Exception(__('Can not create order becausee').' : <br><br> - '.__('The option to authorize the sale of out of stock products is disabled').' <br> - '.__('There is not enough stock available to ship products').' : <br>'.implode('<br>', $notShippable));

                        $productsIdsToShip = array_intersect($shippable, $productsIdsToShip);
                        $this->_shipmentHelper->createShipment($order, $productsIdsToShip);
                    }

                    //Store order manager
                    $userId = $this->_registry->getCurrentUserId();
                    $this->_orderManagerFactory->create()->assignManagerToOrder($order->getId(), $userId);

                    if($this->_config->emailNotificationIsEnabled())
                    {
                        $this->_notification->notifyCustomer($order);
                    }
                }
                catch(\Exception $ex)
                {
                    $order->delete();
                    throw new \Exception($ex->getMessage());
                }

                $result['success'] = true;
                $result['order_id'] = $order->getId();
                $result['action'] = $this->getUrl('pointofsales/sales/view', ['id' => $order->getId(), 'download_receipt' => 1]);
                $result['action_label'] = 'Order #'.$order->getIncrementId();
                $result['msg'] = 'New Order created : '.$order->getIncrementId();
            }
            else
            {
                $result = $this->_quoteHelper->getResult();
                $this->addPerfLog('after get results');

                $this->hydrateResultWithProductDetails($result);
                $this->addPerfLog('after hydrate products');

                $user = $this->getUser();
                $result['user']['id'] = $user->getId();
                $result['user']['name'] = $user->getName();
            }

            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        die(json_encode($result));
    }

    protected function hydrateResultWithProductDetails(&$result)
    {
        foreach($result['items'] as &$item)
        {
            $item['image_url'] = $this->getImage($item['product_id']);
            $item['text_options'] = $this->getOptionsAsText($item);
        }
    }

    protected function getImage($productId)
    {
        $product = $this->_productFactory->create()->load($productId);
        return $this->_productHelper->getImageUrl($product);
    }

    protected function getOptionsAsText($item)
    {
        $options = [];

        $quoteItem = $this->_quoteHelper->getQuoteItem($item['item_id']);

        foreach($this->_productConfig->getCustomOptions($quoteItem) as $optionItem)
        {
            $options[] = $optionItem['label'].' : '.$optionItem['print_value'];
        }

        return implode(', ', $options);
    }

    protected function setGuestCustomer()
    {
        $guestCustomerGroupId = $this->_config->getGuestField('group') ? $this->_config->getGuestField('group') : 0;
        $email = $this->_config->getGuestField('email');
        
        $this->_posRegistry->setCurrentCustomerGroupId($guestCustomerGroupId);
        $this->_quoteHelper->setGuestCustomer($email, $this->getGuestAddress());
    }

    protected function setCustomerData($customerData, $address)
    {
        $customer= $this->_customerRepository->getById($customerData['entity_id']);
        $this->_posRegistry->setCurrentCustomerGroupId($customer->getGroupId());

        $this->_quoteHelper->setCustomer($customer);

        $hasAddress = false;
        foreach($address as $k => $v)
        {
            if ($v)
                $hasAddress = true;
        }

        if (!$hasAddress)
            $address = $this->getGuestAddress();

        $address['street'] = $address['street0'] . "\n" . $address['street1'];
        
        $this->_quoteHelper->setAddress($address);

        return $this;
    }

    protected function getGuestAddress()
    {
        $address = [];

        $address['firstname'] = ($this->_config->getGuestField('firstname')) ? $this->_config->getGuestField('firstname') : '';
        $address['lastname'] = ($this->_config->getGuestField('lastname')) ? $this->_config->getGuestField('lastname') : '';
        $address['street'] = ($this->_config->getGuestField('street')) ? $this->_config->getGuestField('street') : '';
        $address['country_id'] = ($this->_config->getGuestField('country_id')) ? $this->_config->getGuestField('country_id') : $this->_config->getGlobalSetting('general/country/default');
        $address['region_id'] = ($this->_config->getGuestField('region_id')) ? $this->_config->getGuestField('region_id') : '';
        $address['city'] = ($this->_config->getGuestField('city')) ? $this->_config->getGuestField('city') : '';
        $address['postcode'] = ($this->_config->getGuestField('postcode')) ? $this->_config->getGuestField('postcode') : '';
        $address['telephone'] = ($this->_config->getGuestField('telephone')) ? $this->_config->getGuestField('telephone') : '';
        $address['save_in_address_book'] = 0;

        return $address;
    }

    public function getUser()
    {
        $userId = $this->_registry->getCurrentUserId();
        $user = $this->_userFactory->create()->load($userId);
        return $user;
    }

    protected function addPerfLog($msg)
    {
        if ($this->_lastPerfLogTimestamp)
        {
            $duration = microtime(true) - $this->_lastPerfLogTimestamp;
            $msg = $duration." - ".$msg;
        }

        $this->_lastPerfLogTimestamp = microtime(true);

        $this->_logger->log($msg, \BoostMyShop\PointOfSales\Helper\Logger::kLogPerf);
    }
}
