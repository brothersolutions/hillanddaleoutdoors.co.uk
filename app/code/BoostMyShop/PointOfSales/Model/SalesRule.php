<?php

namespace BoostMyShop\PointOfSales\Model;

use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;

class SalesRule
{
    protected $_rule;
    protected $_couponCollection;
    protected $couponFactory;

    public function __construct(
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\SalesRule\Model\ResourceModel\Coupon\Collection $couponCollection,
        \Magento\SalesRule\Model\CouponFactory $couponFactory
    ) {
        $this->_rule = $rule;
        $this->_couponCollection = $couponCollection;
        $this->couponFactory = $couponFactory;

    }

    public function createCoupon($discount, $type, $quoteId, $websiteId)
    {
        $this->deleteExistCouponByQuoteId($quoteId);

        $name = 'POS'.$quoteId;
        $description = 'POS quote Id :'.$quoteId.', Discount :'.$discount.', type :'.$type;
        $flagIsFreeShipping = 'no';
        $couponCode = 'POS'.$quoteId;

        $coupon = $this->_rule->setName($name)
                        ->setDescription($description)
                        ->setFromDate(date('Y-m-d'))
                        ->setToDate('')
                        ->setUsesPerCustomer(1)
                        ->setCustomerGroupIds(array('0','1','2','3',))
                        ->setIsActive(1)
                        ->setSimpleAction($type)
                        ->setDiscountAmount($discount)
                        ->setDiscountQty(1)
                        ->setApplyToShipping($flagIsFreeShipping)
                        ->setTimesUsed(1)
                        ->setWebsiteIds(array($websiteId))
                        ->setCouponType(2)
                        ->setCouponCode($couponCode)
                        ->setUsesPerCoupon(NULL)
                        ->save();

        $ruleById = $this->couponFactory->create()->load( $coupon->getId(),'rule_id');
        $ruleById->setData('pos_quote_id', $quoteId)->save();

        return $ruleById->getCode();
    }

    public function deleteExistCouponByQuoteId($quoteId)
    {
        $couponCode = 'POS'.$quoteId;
        $ruleExistWithPosId = $this->couponFactory->create()->loadByCode($couponCode);

        if($ruleExistWithPosId->getId())
            $ruleExistWithPosId->delete();

        return $this;
    }
}
