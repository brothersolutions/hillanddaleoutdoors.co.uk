<?php

namespace Hiddentechies\Googlecustomerreview\Model\Config\Source;

/**
 * Display mode
 *
 */
class Optinstyles implements \Magento\Framework\Option\ArrayInterface {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'CENTER_DIALOG', 'label' => __('CENTER DIALOG')],
            ['value' => 'BOTTOM_RIGHT_DIALOG', 'label' => __('BOTTOM RIGHT DIALOG')],
            ['value' => 'BOTTOM_LEFT_DIALOG', 'label' => __('BOTTOM LEFT DIALOG')],
            ['value' => 'TOP_RIGHT_DIALOG', 'label' => __('TOP RIGHT DIALOG')],
            ['value' => 'TOP_LEFT_DIALOG', 'label' => __('TOP LEFT DIALOG')],
            ['value' => 'BOTTOM_TRAY', 'label' => __('BOTTOM TRAY')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

}
