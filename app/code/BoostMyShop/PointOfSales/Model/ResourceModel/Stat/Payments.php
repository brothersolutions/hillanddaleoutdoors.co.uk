<?php

namespace BoostMyShop\PointOfSales\Model\ResourceModel\Stat;


class Payments extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('', '');
    }

    public function getItems($storeId, $from, $to)
    {
        $select = $this->getConnection()
            ->select()
            ->from(array('o' => $this->getTable('bms_pointofsales_order_payment')), array('method', new \Zend_Db_Expr('SUM(amount) as total')))
            ->joinLeft($this->getTable('sales_order'), 'order_id = entity_id', array('store_id'))
            ->where('store_id = '.$storeId)
            ->where('o.created_at >= "'.$from.'"')
            ->where('o.created_at <= "'.$to.'"')
            ->where('state NOT IN ("closed", "canceled")')
            ->group('method');

        $result = $this->getConnection()->fetchAll($select);

        return $result;
    }

    public function getTotalSalesInclTax($storeId, $from, $to)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('sales_order'), array(new \Zend_Db_Expr('SUM(base_subtotal) + SUM(base_shipping_amount) + SUM(base_discount_amount) + SUM(discount_tax_compensation_amount) + SUM(base_tax_amount) as sub_total')))
            ->where('store_id = '.$storeId)
            ->where('created_at >= "'.$from.'"')
            ->where('created_at <= "'.$to.'"')
            ->where('state NOT IN ("closed", "canceled")');

        $result = $this->getConnection()->fetchOne($select);

        return $result;
    }
}