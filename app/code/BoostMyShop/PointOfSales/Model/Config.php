<?php

namespace BoostMyShop\PointOfSales\Model;

class Config
{
    /**
     * Core store config
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    protected $_moduleManager;

    /*
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\Manager $moduleManager
    ){
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleManager = $moduleManager;
    }

    public function getSetting($path, $storeId = 0)
    {
        return $this->_scopeConfig->getValue('pointofsales/'.$path, 'store', $storeId);
    }

    public function getGlobalSetting($path, $storeId = 0)
    {
        return $this->_scopeConfig->getValue($path, 'store', $storeId);
    }

    public function isAdvancedStockEnabled()
    {
        return  $this->_moduleManager->isEnabled('BoostMyShop_AdvancedStock');
    }

    // --------------------------------- GENERAL --------------------------------- //
    public function getBarcodeAttribute()
    {
        return $this->_scopeConfig->getValue('pointofsales/general/barcode_attribute');
    }

    public function isSoundDisabled()
    {
        return $this->getSetting('general/disable_sound');
    }

    public function AllowOutOfStockProductSale()
    {
        return $this->getSetting('general/oos_product_sale');
    }
  
    public function areSalesFiltered()
    {
        return $this->getSetting('general/filter_sales');
    }

    public function getSearchMode()
    {
        return $this->getSetting('general/search_mode');
    }

    // --------------------------------- CASH DRAWER --------------------------------- //
    public function isOpeningEnabled()
    {
        return $this->getSetting('cash_drawer/enable_opening');
    }

    public function getCashDrawerMethod()
    {
        return $this->getSetting('cash_drawer/method');
    }

    // --------------------------------- CHECKOUT --------------------------------- //
    public function getDefaultShippingMethod($websiteId)
    {
        return $this->_scopeConfig->getValue('pointofsales/checkout/shipping_method', 'website', $websiteId);
    }

    public function getDefaultPaymentMethod($websiteId)
    {
        $value = $this->_scopeConfig->getValue('pointofsales/checkout/payment_method', 'website', $websiteId);
        if (!$value)
            $value = "multiple_payment";
        return $value;
    }

    public function downloadReceiptAutomatically()
    {
        return $this->getSetting('checkout/automatic_receipt_download');
    }

    // --------------------------------- GUEST CUSTOMER --------------------------------- //
    public function getGuestField($field)
    {
        return $this->getSetting('guest/'.$field);
    }

    // --------------------------------- RECEIPT CONFIGURATION --------------------------------- //
    public function getReceiptWidth($storeId)
    {
        return $this->getSetting('receipt/width', $storeId);
    }

    public function getReceiptHeaderText($storeId)
    {
        return $this->getSetting('receipt/header', $storeId);
    }

    public function getReceiptFooterText($storeId)
    {
        return $this->getSetting('receipt/footer', $storeId);
    }

    public function getLogo($storeId)
    {
        return $this->getSetting('receipt/logo', $storeId);
    }

    // --------------------------------- MULTIPLE PAYMENT --------------------------------- //
    public function getMultiplePaymentMethods()
    {
        return explode(',', $this->_scopeConfig->getValue('payment/multiple_payment/methods'));
    }
  
    public function getChangeMethod()
    {
        return $this->_scopeConfig->getValue('payment/multiple_payment/change_method');
    }
    public function emailNotificationIsEnabled()
    {
        return $this->getSetting('checkout/customer_notification');
    }
}