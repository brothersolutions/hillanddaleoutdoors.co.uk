<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Configuration;

class Flush extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Configuration
{

    public function execute()
    {
        $this->_posRegistry->flushCurrentUserSettings();

        $this->messageManager->addSuccess(__('Customer settings flushed.'));

        $this->_redirect('adminhtml/system_config/edit', ['section' => 'pointofsales']);
    }

}
