<?php

namespace BoostMyShop\PointOfSales\Controller\Order;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Action\Context;

class PrintReceipt extends \Magento\Framework\App\Action\Action
{
    protected $_orderFactory;
    protected $_filesystem;


    public function __construct(
        Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Filesystem $filesystem

    ) {
        parent::__construct($context);
        $this->_orderFactory = $orderFactory;
         $this->_filesystem = $filesystem;
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = $this->_orderFactory->create()->load($orderId);
        $token = $this->getRequest()->getParam('token');
        
        if (($token) && ($token == sha1($order->getId().' order '.$order->getcreated_at())))
        {
            try {
                $pdf = $this->_objectManager->create('BoostMyShop\PointOfSales\Model\Pdf\Receipt')->getPdf([$order]);
                $fileName = 'order_' . $order->getincrement_id(). '.pdf';
                $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                    'receipt_' . $order->getincrement_id(). '.pdf',
                    $pdf->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
                //Delete file
                $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
                $dir->delete($fileName);
            }catch(\Exception $e){} 
        }
    }
}
