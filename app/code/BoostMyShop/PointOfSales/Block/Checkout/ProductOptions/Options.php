<?php
namespace BoostMyShop\PointOfSales\Block\Checkout\ProductOptions;

class Options extends \BoostMyShop\PointOfSales\Block\Checkout\AbstractCheckout
{
    protected $_template = 'Checkout/ProductOptions/Options.phtml';

    protected $_product = null;
    protected $_options = null;

    public function getProduct()
    {
        if ($this->_product == null)
        {
            $productId = $this->_coreRegistry->registry('pos_options_product_id');
            $this->_product = $this->_productFactory->create()->load($productId);
        }
        return $this->_product;
    }

    public function getOptions()
    {
        if ($this->_options == null)
        {
            $this->_options = $this->_productOptionRepository->getProductOptions($this->getProduct());
        }
        return $this->_options;
    }

}