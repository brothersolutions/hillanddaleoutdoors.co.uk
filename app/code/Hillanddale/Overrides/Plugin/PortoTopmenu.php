<?php


namespace Hillanddale\Overrides\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\TreeFactory;
use Mageplaza\LayeredNavigationUltimate\Helper\Data;
use Mageplaza\LayeredNavigationUltimate\Model\Config\Source\ProductPosition;
use Smartwave\Megamenu\Block\Topmenu;
//use Mageplaza\LayeredNavigationUltimate\Plugin\PortoTopmenu

/**
 * Class PortoTopmenu
 *
 * @package Hillanddale\Overrides\Plugin
 */

class PortoTopmenu
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var TreeFactory
     */
    protected $treeFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Topmenu constructor.
     *
     * @param Data $helper
     * @param TreeFactory $treeFactory
     * @param RequestInterface $request
     */
    public function __construct(
        Data $helper,
        TreeFactory $treeFactory,
        RequestInterface $request
    ) {
        $this->helper      = $helper;
        $this->treeFactory = $treeFactory;
        $this->request     = $request;
    }

    /**
     * @param Topmenu $subject
     * @param $categories
     *
     * @return mixed
     */
    public function afterGetStoreCategories(
        Topmenu $subject,
        $categories
    ) {
        $pages = $this->helper->getProductsPageCollection();
        foreach ($pages as $page) {
            if ($this->helper->isEnabled() && $this->helper->canShowProductPageLink($page, ProductPosition::CATEGORY)) {
                $categories->add(
                    new Node(
                        [
                            'name'            => $page->getPageTitle(),
                            'id'              => 'mpblog-node',
                            'url'             => $this->helper->getProductPageUrl($page),
                            'is_active'       => 1,
                            'include_in_menu' => 1
                        ],
                        'id',
                        $this->treeFactory->create()
                    )
                );
            }
        }
        return $categories;
    }
}
