<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace ThinkCommerce\Configurableoption\Model\ConfigurableProduct;

use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute;

/**
 * Class ConfigurableAttributeData
 * @api
 * @since 100.0.2
 */
class ConfigurableAttributeData extends \Magento\ConfigurableProduct\Model\ConfigurableAttributeData
{
    /**
     * Get product attributes
     *
     * @param Product $product
     * @param array $options
     * @return array
     */
    public function getAttributesData(Product $product, array $options = [])
    {
        $defaultValues = [];
        $attributes = [];

        
        foreach ($product->getTypeInstance()->getConfigurableAttributes($product) as $attribute) {
            
            $attributeOptionsData = $this->getAttributeOptionsData($attribute, $options);

            /////////////////////////////////////////
           
            if ($attributeOptionsData) {
                $all_attributes=[];
                $i = 0;
                $productAttribute = $attribute->getProductAttribute();

                $attr = $product->getResource()->getAttribute($productAttribute->getAttributeCode());
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $_children = $product->getTypeInstance()->getUsedProducts($product);
                $stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');

            
                $available_options = [];
                foreach ($_children as $key => $value) {
                    // $_children_prod=$product->load($value->getId());
                    $_children_prod = $objectManager->create('Magento\Catalog\Model\Product')->load($value->getId());
                    $child_optionId = $_children_prod->getData($productAttribute->getAttributeCode());                
                    $available_options[$child_optionId]['value_index'] = $child_optionId;

                    if ($attr->usesSource()) {
                       $optionText = $attr->getSource()->getOptionText($child_optionId);
                    }
                    $available_options[$child_optionId]['label'] = $optionText; 

                    $stockitem = $stockRegistry->getStockItem($value->getId(), $value->getStore()->getWebsiteId());
                    $stockStatus=1;
                    if($stockitem->getQty() <= 0){
                        $stockStatus=0;
                    };                         
                    $available_options[$child_optionId]['stockStatus'][$value->getId()] = $stockStatus;
                }   

                foreach($available_options as $optionId=>$value){
                    if(in_array("1",$available_options[$optionId]['stockStatus'])){
                        $option_stock = 1;
                    }else{
                        $option_stock = 0;
                    }  
                    $available_options[$optionId]['optionStockStatus'] = $option_stock;      
                }

               

                foreach ($available_options as $attributeOption) {
                    // print_r($attributeOption);
                    $optionId = $attributeOption['value_index'];
                    
                    $all_attributes[$i]['id'] = $optionId;
                    $all_attributes[$i]['label'] = $attributeOption['label'];
                    $j=0;
                    ////////////////////////////////////////////////////////
                    // $attr = $product->getResource()->getAttribute($productAttribute->getAttributeCode());

                    foreach ($_children as $key => $value) {
                        // $_children_prod=$product->load($value->getId());
                        $_children_prod = $objectManager->create('Magento\Catalog\Model\Product')->load($value->getId());
                        $child_optionId = $_children_prod->getData($productAttribute->getAttributeCode());
                        
                        $stockitem = $stockRegistry->getStockItem($value->getId(), $value->getStore()->getWebsiteId());
                        $stockStatus=1;
                        if($stockitem->getQty() <= 0){
                            $stockStatus=0;
                        };

                        if ($optionId==$child_optionId) {
                            if ($attributeOption['optionStockStatus'] == 1) {
                                if ($stockStatus==1) {
                                    $all_attributes[$i]['products'][] = $value->getId();
                                }
                                $all_attributes[$i]['optionStockStatus'] = 1;
                            }else{
                                $all_attributes[$i]['products'][] = $value->getId();
                                $all_attributes[$i]['optionStockStatus'] = 0;
                            }

                            
                        }

                    }
                    /////////////////////////////////////////////////////////

                    $i++;
                }

                

                // print_r($all_attributes);
                    /*$all_attributes[$optionId]['optionStockStatus'] =1;

                foreach($all_attributes as $optionId=>$value){
                    if(in_array("1",$all_attributes[$optionId]['stockStatus'])){
                        $option_stock = 1;
                    }else{
                        $option_stock = 0;
                    }  
                    $all_attributes[$optionId]['optionStockStatus'] = $option_stock;      
                }*/
             }

            // print_r($all_attributes);
            

            ///////////////////////////////////////



            if ($attributeOptionsData) {
                $productAttribute = $attribute->getProductAttribute();
                $attributeId = $productAttribute->getId();
                $attributes[$attributeId] = [
                    'id' => $attributeId,
                    'code' => $productAttribute->getAttributeCode(),
                    'label' => $productAttribute->getStoreLabel($product->getStoreId()),
                    // 'options' => $attributeOptionsData,
                    'options'=> $all_attributes,
                    'position' => $attribute->getPosition(),
                ];
                $defaultValues[$attributeId] = $this->getAttributeConfigValue($attributeId, $product);
            }
            
        }
        return [
            'attributes' => $attributes,
            'defaultValues' => $defaultValues,
        ];
    }

    /**
     * @param Attribute $attribute
     * @param array $config
     * @return array
     */
    protected function getAttributeOptionsData($attribute, $config)
    {
        $attributeOptionsData = [];
        foreach ($attribute->getOptions() as $attributeOption) {
            $optionId = $attributeOption['value_index'];
            $attributeOptionsData[] = [
                'id' => $optionId,
                'label' => $attributeOption['label'],
                'products' => isset($config[$attribute->getAttributeId()][$optionId])
                    ? $config[$attribute->getAttributeId()][$optionId]
                    : [],
            ];
        }
        return $attributeOptionsData;
    }

    /**
     * @param int $attributeId
     * @param Product $product
     * @return mixed|null
     */
    protected function getAttributeConfigValue($attributeId, $product)
    {
        return $product->hasPreconfiguredValues()
            ? $product->getPreconfiguredValues()->getData('super_attribute/' . $attributeId)
            : null;
    }
}
