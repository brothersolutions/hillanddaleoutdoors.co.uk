<?php

namespace BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout;

class Shortcuts extends \BoostMyShop\PointOfSales\Controller\Adminhtml\Checkout
{

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $result = [];

        try
        {
            $result['shortcuts_price'] = $this->getShortcutsPrices($data['customer_group_id']);
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        die(json_encode($result));
    }

    public function getShortcutsPrices($customerGroupId)
    {
        $shortcutsPrices = array();
        $storeId = $this->_posRegistry->getCurrentStoreId();

        $collection = $this->_productCollectionFactory->create();
        $collection->addFieldToFilter('type_id', ['in' => ['simple']]);
        $collection->addAttributeToFilter('pos_shortcut', 1);
        $collection->setStoreId($storeId);
        $collection->addFinalPrice();

        if($collection->getSize() > 0){

            $collection->addAttributeToSelect('name');
            $collection->addAttributeToSelect('price');

            foreach($collection as $product)
            {
                $productId = $product->getId();
                $finalPrice = $this->_productInformation->getProductFinalPrice($product, $customerGroupId);

                $shortcutsPrices[$productId] = $finalPrice;
            }
        }

        return $shortcutsPrices;
    }

}
