<?php

namespace BoostMyShop\PointOfSales\Model\Source;

class ProductSearchMethods implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        $options = array();

        $options[] = array('value' => 'keyboard_typing', 'label' => __('On keyboard typing'));
        $options[] = array('value' => 'search_button', 'label' => __('On search button click'));

        return $options;
    }

}
