<?php
namespace BoostMyShop\PointOfSales\Block\Checkout;

class Search extends AbstractCheckout
{
    protected $_template = 'Checkout/Search.phtml';

    public function getSearchMode()
    {
        return $this->_config->getSearchMode();
    }
}