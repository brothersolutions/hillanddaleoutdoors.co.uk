define([
    "jquery",
    "mage/translate",
    "prototype",
    "Magento_Ui/js/modal/alert"
], function(jQuery, translate, prototype, alert){

    window.AdminPosCheckout = new Class.create();

    AdminPosCheckout.prototype = {

        initialize: function(){

            this.products = {};
            this.customer = {};
            this.shippingMethod = {};
            this.paymentMethod = {};
            this.result;
            this.quoteId = null;
            this.storeId = null;

            this.openingDialog;
            this.shippingDialog;
            this.paymentDialog;
            this.shortcutDialog;
            this.searchProductDialog;
            this.customerDialog;
            this.storeDialog;
            this.userDialog;
            this.productEditDialog;
            this.productOptionsDialog;

            this.visiblePaymentFormCode = null;

            this.productOptionsUrl = null;
        },

        reset: function()
        {
            this.products = {};
            this.customer = {};
            this.quoteId = null;

            // Reset 'Create new customer' fields
            jQuery('#new_customer_group_id').val("");
            jQuery('#new_customer_email').val("");
            jQuery('#new_customer_address_firstname').val("");
            jQuery('#new_customer_address_lastname').val("");
            jQuery('#new_customer_taxvat').val("");

            //Load and set Guest data in the 'Customer details' tab

            jQuery('#customer_information_mode').val('guest');
            jQuery('#customer_information_id').val("");
            jQuery('#customer_information_email').val("");
            jQuery('#customer_information_taxvat').val("");
            jQuery('#customer_information_group_id').val("");
            jQuery('#customer_information_website_id').val("");

            jQuery('#customer_address_firstname').val("");
            jQuery('#customer_address_lastname').val("");
            jQuery('#customer_address_company').val("");
            jQuery('#customer_address_street0').val("");
            jQuery('#customer_address_street1').val("");
            jQuery('#customer_address_city').val("");
            jQuery('#customer_address_country_id').val("");
            jQuery('#customer_address_region_id').val("");
            jQuery('#customer_address_postcode').val("");
            jQuery('#customer_address_telephone').val("");
            jQuery('#edit_discount').val("");
            jQuery('#edit_discount_div').html("&nbsp;");

            objPosCheckout.selectCustomer('guest', function(){
                objPosCheckout.refreshQuote(false);
            });
        },

        configure: function(refreshUrl, productEmptyLayoutUrl, searchProductUrl, customerInformationUrl, createCustomerUrl, changeStoreUrl, changeUserUrl, productIdFromBarcodeUrl, saveOpeningUrl, productOptionsUrl, shortcutInformationUrl)
        {
            this.refreshUrl = refreshUrl;
            this.searchProductUrl = searchProductUrl;
            this.productEmptyLayoutUrl = productEmptyLayoutUrl;
            this.customerInformationUrl = customerInformationUrl;
            this.createCustomerUrl = createCustomerUrl;
            this.changeStoreUrl = changeStoreUrl;
            this.changeUserUrl = changeUserUrl;
            this.saveOpeningUrl = saveOpeningUrl;
            this.productIdFromBarcodeUrl = productIdFromBarcodeUrl;
            this.productOptionsUrl = productOptionsUrl;
            this.shortcutInformationUrl = shortcutInformationUrl;

            this.initEmptyProductLayout();
        },

        initEmptyProductLayout: function()
        {
            var data = {FORM_KEY: window.FORM_KEY};
            jQuery.ajax({
                url: objPosCheckout.productEmptyLayoutUrl,
                data: data,
                success: function (resp) {
                    objPosCheckout.productEmptyLayout = resp;
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        addProductFromBarcode: function(barcode)
        {
            var data = {barcode: barcode, FORM_KEY: window.FORM_KEY};

            jQuery.ajax({
                url: objPosCheckout.productIdFromBarcodeUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        objPosUi.playNok();
                        alert({content: resp.message});
                    }
                    else {
                        if (resp.has_options == "0")
                            objPosCheckout.addProduct(resp.product_id, 1);
                        else
                            objPosCheckout.showProductOptionsPopup(resp.product_id);
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        addProduct: function (productId, qty, options, preventQuoteRefresh)
        {
            if (!this.products[productId])
                this.products[productId] = {qty: 0, options: {}, custom_price: null, ship_later: 0, comment: null, discount: 0};

            this.products[productId].qty += qty;

            objPosUi.playOk();

            if (!preventQuoteRefresh)
                this.refreshQuote(false);
        },


        editProduct: function(productId, qty, customPrice, shipLater, comment, discount)
        {
            this.products[productId].qty = qty;
            this.products[productId].custom_price = customPrice;
            this.products[productId].ship_later = shipLater;
            this.products[productId].comment = comment;
            this.products[productId].discount = discount;

            this.productEditDialog.modal('closeModal');

            this.refreshQuote(false);
        },

        getItemByProductId: function(productId)
        {
            var returnedItem;
            this.result.items.forEach(function(item) {
                if (item.product_id == productId)
                    returnedItem = item;
            });
            return returnedItem;
        },

        showEditProductPopup: function (productId)
        {
            this.productEditDialog = jQuery('#product_edit_popup').modal({
                title: translation["editProduct"],
                type: 'slide',
                buttons: []
            });

            var product = this.getItemByProductId(productId);
            jQuery('#edit_product_title').html(product.name);
            jQuery('#edit_product_sku').html(product.sku);
            jQuery('#edit_product_options').html(product.text_options);
            jQuery('#edit_product_image').attr('src', product.image_url);
            jQuery('#edit_product_id').val(productId);

            jQuery('#edit_product_qty').val(product.qty);
            jQuery('#edit_product_qty_div').html(product.qty);

            jQuery('#edit_product_price').val(product.price);
            jQuery('#edit_product_price_div').html(product.price);

            if (product.ship_later == 1)
            {
                jQuery('#div_edit_product_ship_later').removeClass("switch-toggle-off");
                jQuery('#div_edit_product_ship_later').addClass("switch-toggle-on");
                jQuery('#edit_product_ship_later').prop('checked',true);
            }
            else
            {
                jQuery('#div_edit_product_ship_later').removeClass("switch-toggle-on");
                jQuery('#div_edit_product_ship_later').addClass("switch-toggle-off");
                jQuery('#edit_product_ship_later').prop('checked',false);
            }
            jQuery('#edit_product_comment').val(product.comment);

            jQuery('#edit_product_discount').val(product.discount_amount);
            jQuery('#edit_product_discount_div').html(product.discount_amount);
            jQuery('#edit_product_discount_currency').html(this.result.currency_format.replace('%s',''));

            this.productEditDialog.modal('openModal');

        },

        showProductOptionsPopup: function(productId)
        {
            this.productOptionsDialog = jQuery('#product_options_popup').modal({
                title: translation["productOptions"],
                type: 'slide',
                buttons: []
            });

            var data = {
                FORM_KEY: window.FORM_KEY,
                product_id: productId
            };


            jQuery.ajax({
                url: this.productOptionsUrl,
                data: data,
                success: function (resp) {
                    jQuery('#product_options_div_content').html(resp);
                },
                failure: function (resp) {
                    jQuery('#product_options_div_content').html(translate("errorOccurred"));
                }
            });

            this.productOptionsDialog.modal('openModal');

        },

        addProductWithOptions: function(productId)
        {
            var options = {};
            var fields = $('product_options_div_content').select("[data-type='option']");
            jQuery.each( fields, function( key, value ) {

                //exclude unchecked checkbox
                var exclude = false;
                if ((value.type == "checkbox") && (!value.checked))
                    exclude = true;

                if (!exclude)
                {
                    if (!options[value.name])
                        options[value.name] = value.value;
                    else
                    {
                        //multiple values case
                        if (Array.isArray(options[value.name]))
                            options[value.name].push(value.value);
                        else
                        {
                            options[value.name] = [options[value.name], value.value];
                        }
                    }
                }
            });

            this.products[productId] = {qty: 1, options: options, custom_price: null, ship_later: 0};

            this.productOptionsDialog.modal('closeModal');

            this.refreshQuote(false);

        },

        removeProduct: function(productId)
        {
            this.products[productId].qty = 0;
            jQuery('#product_' + productId).remove();
            this.refreshQuote(false);

            var e = window.event;
            e.cancelBubble = true;
            if (e.stopPropagation)
                e.stopPropagation();
        },

        decreaseQuantity: function(productId)
        {
            this.products[productId].qty -= 1;
            if (this.products[productId].qty == 0) {
                jQuery('#product_' + productId).remove();
            }
            this.refreshQuote(false);
        },

        increaseQuantity: function(productId)
        {
            this.products[productId].qty += 1;
            this.refreshQuote(false);
        },

        selectCustomer : function(customerId, callback) {
            this.getCustomerInformation(customerId, function(data){
                var resp = data;

                jQuery('#customer_information_mode').val(resp.customer.mode);
                jQuery('#customer_information_id').val(resp.customer.entity_id);
                jQuery('#customer_information_email').val(resp.customer.email);
                jQuery('#customer_information_taxvat').val(resp.customer.taxvat);
                jQuery('#customer_information_group_id').val(resp.customer.group_id);
                jQuery('#customer_information_website_id').val(resp.customer.website_id);

                if (resp.address)
                {
                    jQuery('#customer_address_firstname').val(resp.address.firstname);
                    jQuery('#customer_address_lastname').val(resp.address.lastname);
                    jQuery('#customer_address_company').val(resp.address.company);
                    jQuery('#customer_address_country_id').val(resp.address.country_id);
                    if(window.updater){
                        window.updater.update()
                    }
                    jQuery('#customer_address_region_id').val(resp.address.region_id);
                    if (resp.address.street)
                    {
                        var street = resp.address.street.split("\n");
                        jQuery('#customer_address_street0').val(street[0]);
                        if(street[1])
                            jQuery('#customer_address_street1').val(street[1]);
                    }
                    else
                    {
                        jQuery('#customer_address_street0').val("");
                        jQuery('#customer_address_street1').val("");
                    }

                    jQuery('#customer_address_city').val(resp.address.city);
                    jQuery('#customer_address_postcode').val(resp.address.postcode);
                    jQuery('#customer_address_telephone').val(resp.address.telephone);
                }

                if(!jQuery('#customer_address_country_id').val()){
                    if (resp.default_country_id) {
                        jQuery('#customer_address_country_id').val(resp.default_country_id);
                    }
                }

                jQuery("#page_tabs_tab_current").click();
                objPosCheckout.refreshShortcutsPrices(resp.customer.group_id);
                callback();
            });
        },

        getCustomerInformation : function(customerId, callback)
        {
            var data = {customer_id: customerId, FORM_KEY: window.FORM_KEY};

            return jQuery.ajax({
                url: this.customerInformationUrl,
                data: data,
                cache: false,
                dataType: 'json',
                success: callback,
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        createCustomer: function()
        {
            fields = $('fieldset_create_customer').select('input', 'select', 'textarea');
            var data = Form.serializeElements(fields, true);
            jQuery.ajax({
                url: this.createCustomerUrl,
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        objPosUi.playNok();
                        alert({content: resp.message});
                    }
                    else
                    {
                        objPosCheckout.selectCustomer(resp.customer_id, function(){
                            objPosCheckout.refreshQuote(false);
                        });
                        jQuery("#page_tabs_tab_current").click();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        refreshQuote: function(createOrder)
        {
            jQuery('#pos_working').html('...');

            var fields = [];
            if ($('payment_form_' + objPosCheckout.paymentMethod))
                fields = $('payment_form_' + objPosCheckout.paymentMethod).select('input', 'select', 'textarea');
            var data = Form.serializeElements(fields, true);

            var customerData = {};
            var customerFieldSet = $('fieldset_customer_information');
            if(customerFieldSet)
            {
                fields = customerFieldSet.select('input', 'select', 'textarea');
                jQuery.each( fields, function( key, value ) {
                    customerData[value.name] = value.value;
                });
            }

            var addressData = {};
            var customerAddressFieldSet = $('fieldset_customer_address');
            if(customerAddressFieldSet)
            {
                fields = customerAddressFieldSet.select('input', 'select', 'textarea');
                jQuery.each( fields, function( key, value ) {
                    addressData[value.name] = value.value;
                });
            }

            data.products = this.products;
            data.customer = customerData;
            data.address = addressData;
            data.form_key = window.FORM_KEY;

            data.quote_id = this.quoteId;
            data.store_id = this.storeId;
            data.shipping_method = this.shippingMethod;
            data.payment_method = this.paymentMethod;
            data.create_order = createOrder;
            data.create_shipment = jQuery('#pos_create_shipment').is(':checked');
            data.create_invoice = jQuery('#pos_create_invoice').is(':checked');
            data.coupon_code = jQuery('#pos_coupon_code').val();
            data.global_discount = jQuery('#edit_discount').val();
            data.selected_type = jQuery('#selected_type').val();

            jQuery.ajax({
                url: this.refreshUrl,
                data: data,
                method: "POST",
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        objPosUi.playNok();
                        alert({content: resp.message});
                    }
                    else
                    {
                        objPosCheckout.result = resp;

                        if (resp.order_id)
                        {
                            //alert({title: 'Confirmation', content: resp.msg});
                            objPosCheckout.reset();
                            objPosUi.showScreen('pos-content-orderconfirmation', resp.action_label, resp.action, true);
                        }
                        else
                        {
                            //update display
                            objPosCheckout.quoteId = objPosCheckout.result.quote_id;
                            objPosCheckout.updateDisplay();
                        }
                    }

                    jQuery('#pos_working').html('');
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                    jQuery('#pos_working').html('');
                }
            });

        },

        updateDisplay: function()
        {
            //totals
            //jQuery('#pos_sub_total').html(objPosCheckout.currencyFormat(this.result.totals.subtotal));
            jQuery('#pos_create_order_button').html(translation["pay"] + objPosCheckout.currencyFormat(this.result.totals.grand_total));

            //products list
            this.result.items.forEach(function(item) {

                //add item to screen
                if (jQuery('#product_' + item.product_id).length == 0)
                {
                    var html = objPosCheckout.getProductLayout(item);
                    if (objPosCheckout.result.items.length == 1)
                        jQuery('#products_container').html('');
                    jQuery('#products_container').html(jQuery('#products_container').html() + html);
                }

                //update details
                jQuery('#product_' + item.product_id + '_price').html(objPosCheckout.currencyFormat(item.price_incl_tax - item.discount_amount));
                jQuery('#product_' + item.product_id + '_qty').html(item.qty + 'x' + (item.ship_later == 1 ? ' <i> ('+translation["shipLater"]+') </i> ' : ''));

            });

            //if quote empty
            if (this.result.items.length == 0)
                jQuery('#products_container').html('<p>&nbsp;</p><p>&nbsp;</p><center>'+ translation["noProductInCart"] +'<br>'+ translation["addProductInCart"] +'</center>');

            //customer information
            jQuery('#pos_customer').html(this.result.customer.title);

            //shipping method
            if (this.result.shipping.title)
                jQuery('#pos_shipping').html(this.result.shipping.title + ' ' + objPosCheckout.currencyFormat(this.result.shipping.grand_total));
            else
                jQuery('#pos_shipping').html(translation["noShippingAvailable"]);

            //payment method
            jQuery('#pos_payment_method').html(this.result.payment.title);
            jQuery('#div_payment_form').html('<div id="payment_form">' + this.result.payment.form + '</form>');
            jQuery('#payment_form_' + this.result.payment.method).show();

            //store & user
            jQuery('#current_store').html(this.result.store.website + '<br>' + this.result.store.group + '<br>' + this.result.store.name);
            jQuery('#current_user').html(this.result.user);

            jQuery('#pos_coupon_code').val(this.result.coupon_code);
            objPosUi.initMultiplePayment(this.result.currency_format, this.result.totals.grand_total);

        },

        getProductLayout: function(productData)
        {
            var html = this.productEmptyLayout;
            html = html.replace('{sku}', productData.sku + ' (#' + productData.product_id + ')');
            html = html.replace('{name}', productData.name);
            html = html.replace('{image_url}', productData.image_url);
            html = html.replace('{options}', (productData.text_options ? productData.text_options : ''));
            html = html.replace(new RegExp("\{id\}", "g"), productData.product_id);
            return html;
        },

        showPaymentMethodsPopup: function()
        {
            if  (!(this.result.items.length > 0))
            {
                objPosUi.playNok();
                alert({title: 'Attention', content: translation["addProduct"]});
                return;
            }

            if (!this.result.shipping.method)
            {
                objPosUi.playNok();
                alert({title: 'Attention', content: translation["selectShippingMethodWarning"]});
                return;
            }

            var hasProductsDiscount = false;
            var products = this.products;
            this.result.items.forEach(function(item, i) {
                if(products[parseInt(item.product_id)].discount>0) {
                    hasProductsDiscount = true;
                }
            });
            if(hasProductsDiscount) {
                jQuery('.global-discount').hide();
                jQuery('.global-discount-note').show();
            }else{
                jQuery('.global-discount').show();
                jQuery('.global-discount-note').hide();
            }

            this.paymentDialog = jQuery('#payment_method_popup').modal({
                title: translation["payment"],
                type: 'slide',
                buttons: []
            });

            //populate with payment method
            jQuery('#pos_payment_buttons').html('');
            jQuery('#pos_payment_forms').html('');
            this.result.payment.available_methods.forEach(function(item) {
                var button = '<br><input type="button" id="button_payment_method_' + item.code + '" class="pos_payment_method_button" value="' + item.title + '" onclick="objPosCheckout.setPaymentMethod(\'' + item.code + '\');">';
                var form = item.form;
                jQuery('#pos_payment_buttons').html(jQuery('#pos_payment_buttons').html() + button);
                jQuery('#pos_payment_forms').html(jQuery('#pos_payment_forms').html() + form);
            });

            if (this.result.payment.available_methods.length == 0)
                jQuery('#payment_method_popup').html(translation["noPaymentMethodAvailable"]);
            else
            {
                this.showCurrentPaymentMethodForm(this.result.payment.method);
                objPosUi.initMultiplePayment(this.result.currency_format, this.result.totals.grand_total);
            }

            this.paymentDialog.modal('openModal');
        },

        showCustomerPopup: function()
        {
            this.customerDialog = jQuery('#customer_popup').modal({
                title: translation["customer"],
                type: 'slide',
                buttons: []
            });

            this.customerDialog.modal('openModal');
        },

        showStorePopup: function()
        {
            this.storeDialog = jQuery('#store_popup').modal({
                title: translation["selectStore"],
                type: 'slide',
                buttons: []
            });

            //highlight current store
            var fields = $('store_popup').select('input');
            jQuery.each( fields, function( key, value ) {
                if (value.id != 'button_store_' + objPosCheckout.result.store.id)
                    jQuery('#' + value.id).removeClass('pos_store_button_selected');
                else
                    jQuery('#' + value.id).addClass('pos_store_button_selected');
            });

            this.storeDialog.modal('openModal');
        },

        selectStore: function(storeId)
        {
            var data = {store_id: storeId};
            jQuery.ajax({
                url: this.changeStoreUrl,
                data: data,
                method: "POST",
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success) {
                        objPosUi.playNok();
                        alert({content: resp.message});
                    }
                    else
                    {
                        objPosCheckout.storeDialog.modal('closeModal');
                        location.reload();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });

        },

        selectUser: function(userId)
        {
            var data = {user_id: userId};
            jQuery.ajax({
                url: this.changeUserUrl,
                data: data,
                method: "POST",
                dataType: 'json',
                success: function (resp) {
                    if (!resp.success)
                    {
                        alert({content: resp.message});
                        objPosUi.playNok();
                    }
                    else
                    {
                        objPosUi.playOk();
                        objPosCheckout.userDialog.modal('closeModal');
                        location.reload();
                    }
                },
                failure: function (resp) {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        showUserPopup: function()
        {
            this.userDialog = jQuery('#user_popup').modal({
                title: translation["selectUser"],
                type: 'slide',
                buttons: []
            });

            //highlight current user
            var fields = $('user_popup').select('input');
            jQuery.each( fields, function( key, value ) {
                if (value.id != 'button_user_' + objPosCheckout.result.user.id)
                    jQuery('#' + value.id).removeClass('pos_user_button_selected');
                else
                    jQuery('#' + value.id).addClass('pos_user_button_selected');
            });

            this.userDialog.modal('openModal');
        },

        setPaymentMethod: function(method)
        {
            this.paymentMethod = {method: method};
            this.showCurrentPaymentMethodForm(method);
        },

        showCurrentPaymentMethodForm: function(method)
        {
            var divId = 'payment_form_' + method;

            if (this.visiblePaymentFormCode)
            {
                jQuery('#payment_form_' + this.visiblePaymentFormCode).hide();
                jQuery('#button_payment_method_' + this.visiblePaymentFormCode).removeClass('pos_payment_method_button_selected');
                this.visiblePaymentFormCode = null;
            }

            if (jQuery('#' + divId)) {
                jQuery('#button_payment_method_' + method).addClass('pos_payment_method_button_selected');
                jQuery('#' + divId).show();
                this.visiblePaymentFormCode = method;
                this.paymentMethod = method;
            }
        },

        showShippingMethodsPopup: function()
        {
            this.shippingDialog = jQuery('#shipping_method_popup').modal({
                title: translation["selectShippingMethod"],
                type: 'slide',
                buttons: []
            });

            //populate with shipping method
            jQuery('#shipping_method_popup').html('');
            this.result.shipping.available_methods.forEach(function(item) {
                var style = 'pos_shipping_method_button';
                if (item.method == objPosCheckout.result.shipping.method)
                    style += ' pos_payment_method_button_selected';
                var html = '<br><input type="button" class="' + style + '" value="' + item.title + '" onclick="objPosCheckout.setShippingMethod(\'' + item.method + '\');">';
                jQuery('#shipping_method_popup').html(jQuery('#shipping_method_popup').html() + html);
            });

            if (this.result.shipping.available_methods.length == 0)
                jQuery('#shipping_method_popup').html(translation["noRatesAvailable"]);

            this.shippingDialog.modal('openModal');
        },

        setShippingMethod: function(method)
        {
            this.shippingMethod = {method: method};
            this.refreshQuote(false);
            this.shippingDialog.modal('closeModal');
        },

        showShortcutsPopup: function()
        {
            this.shortcutDialog = jQuery('#shortcuts_popup').modal({
                title: translation["shortcuts"],
                type: 'slide',
                buttons: []
            });

            this.shortcutDialog.modal('openModal');
        },

        refreshShortcutsPrices: function(customerGroupId)
        {
            var data = {customer_group_id: customerGroupId, FORM_KEY: window.FORM_KEY};

            jQuery.ajax({
                url: this.shortcutInformationUrl,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (resp) {
                    if (!resp.success)
                        alert({content: resp.message});
                    else
                    {
                        var shortcutsPrices = resp.shortcuts_price;
                        for(var productId in shortcutsPrices)
                        {
                            var price = objPosCheckout.currencyFormat(shortcutsPrices[productId]);
                            jQuery('#pos_shortcut_product_price_' + productId).html(price);
                        }
                    }
                },
                failure: function () {
                    jQuery('#debug').html(translation["errorOccurred"]);
                }
            });
        },

        closeShortcutPopup: function()
        {
            this.shortcutDialog.modal('closeModal');
        },

        showSearchProductPopup: function()
        {
            this.searchProductDialog = jQuery('#search_product_popup').modal({
                title: translation["searchForProducts"],
                type: 'slide',
                buttons: []
            });

            this.searchProductDialog.modal('openModal');
            objPosUi.playOk();
            setTimeout(function(){ jQuery('#pos_search_product_field').focus(); }, 500);
        },

        showOpeningPopup: function()
        {
            objPosUi.showNumericPopup(translation["enterOpeningValue"], '', function(value) {objPosCheckout.saveOpeningValue(value);});
        },

        saveOpeningValue: function(value)
        {
            if (jQuery('#stat_payment_opening'))
                jQuery('#stat_payment_opening').html(value);

            var data = {opening_value: value};
            jQuery.ajax({
                url: this.saveOpeningUrl,
                data: data,
                success: function (resp) {

                }
            });
        },

        closeSearchProductPopup: function()
        {
            this.searchProductDialog.modal('closeModal');
        },

        displaySearchResults: function()
        {
            var searchString = jQuery('#pos_search_product_field').val();
            if (searchString.length <= 2)
                return;

            jQuery('#pos_search_results').html('<div style="clear: both;"></div><p>&nbsp;</p>Please wait...');
            var data = {search_string: searchString};
            jQuery.ajax({
                url: this.searchProductUrl,
                data: data,
                success: function (resp) {
                    jQuery('#pos_search_results').html(resp);
                }
            });
        },

        commit: function()
        {
            this.paymentDialog.modal('closeModal');
            this.refreshQuote(true);
        },

        currencyFormat: function(value)
        {
            value = parseFloat(value).toFixed(2);
            if(this.result && this.result.currency_format){
                value = this.result.currency_format.replace('%s', value);
            }
            return value;
        },

        applyCustomerMode: function()
        {
            switch(jQuery('#customer_information_mode').val())
            {
                case 'guest':
                    objPosCheckout.selectCustomer('guest', function(){
                        objPosCheckout.refreshQuote(false);
                    });
                    break;
            }
        }

    };

});
