<?php

namespace BoostMyShop\PointOfSales\Model\Plugin\Quote;


class PosCommentToOrderItem
{

    public function aroundConvert(\Magento\Quote\Model\Quote\Item\ToOrderItem $subject, \Closure $proceed, \Magento\Quote\Model\Quote\Item\AbstractItem $item, $additional = [] )
    {
        /** @var $orderItem Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setpos_comment($item->getpos_comment());
        return $orderItem;
    }

}