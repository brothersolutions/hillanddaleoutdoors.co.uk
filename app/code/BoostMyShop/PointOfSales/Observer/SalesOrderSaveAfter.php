<?php

namespace BoostMyShop\PointOfSales\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class SalesOrderSaveAfter implements ObserverInterface
{
    protected $_payment;
    protected $_salesRule;
    protected static $_processedOrders = [];

    public function __construct(
        \BoostMyShop\PointOfSales\Model\Payment $payment,
        \BoostMyShop\PointOfSales\Model\SalesRule $salesRule
    ) {
        $this->_payment = $payment;
        $this->_salesRule = $salesRule;
    }

    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quoteId = $order->getQuoteId();
        if($quoteId)
            $this->_salesRule->deleteExistCouponByQuoteId($quoteId);
        if ($order->getOrigData('entity_id'))
            return $this;
        if (in_array($order->getId(), self::$_processedOrders))
            return $this;

        self::$_processedOrders[] = $order->getId();
        $this->_payment->createForOrder($order);

        return $this;
    }

}
